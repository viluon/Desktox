
-- Desktox
--  A set of graphics libraries for ComputerCraft by @viluon <viluon@espiv.net>

--   desktox
--    The Desktox entry point

--    This Source Code Form is subject to the terms of the Mozilla Public
--    License, v. 2.0. If a copy of the MPL was not distributed with this
--    file, You can obtain one at http://mozilla.org/MPL/2.0/.

-- luacheck:globals fs shell
-- luacheck:ignore require

if not require and fs then
	dofile "/symlink/desktox/require.lua"
	--local path = fs.getDir( shell.getRunningProgram and shell.getRunningProgram() or shell.dir() )
	--dofile( fs.combine( path, "require.lua" ) )
end

-- Utilities
local string_match  = string. match
local string_gmatch = string.gmatch

local table_sort = table.sort

local tostring = tostring

-- Constants
local DEFAULT_VID = 64

-- Tags (for the variant engine)
local platform_tags = {
	"lua51", "lua52", "lua53", "jit20", "jit21", "terra",

	"pre53", "anyarch",
}

local n_platform_tags = #platform_tags

---- Also make this a lookup table
for i = 1, n_platform_tags do
	platform_tags[ platform_tags[ i ] ] = true
end

local alias_tags = {
	pre52   = { penalty = 8; "lua51", "jit20", "jit21", "terra" };
	pre53   = { penalty = 8; "lua51", "lua52", "jit20", "jit21", "terra" };
	anyarch = { penalty = 8; "lua51", "lua52", "lua53", "jit20", "jit21", "terra", };
}

local desktox = {}
local api_methods = {}
local api_metatable = {}

local empty_fn  = function()end
local empty_tbl = setmetatable( {}, {
	__newindex  = empty_fn;
} )

-- Functions for method name conversion
local confunctions
confunctions = {
	snake = function( str )
		-- snake_case rules! That's why Desktox has it as the default
		return str
	end;
	camel = function( str )
		return str:gsub( "_([^_])", string.upper ):gsub( "_", "" )
	end;
	title = function( str )
		return confunctions.camel( "_" .. str )
	end;
}

--- Parse a variant name.
-- @param name	description
-- @return The method name, ID, the number of tags, and a sorted array of its tags
local function parse_variant( name )
	local method, ID, tags = string_match( name, "^([%w_]+)_v(%d*)([%w_]*)$" )

	if not method or not ID or not tags then
		error( "Invalid variant format: '" .. name .. "'", 3 )
	end

	if ID == '' then
		ID = DEFAULT_VID
	end

	local n_tags = 0
	local tags_array = {}

	for tag in string_gmatch( tags, "_([%w]+)" ) do
		n_tags = n_tags + 1
		tags_array[ n_tags ] = tag
	end

	table_sort( tags_array )

	return method, tonumber( ID ), n_tags, tags_array
end

--- Find a variant matching the conditions.
-- @param cooked_methods	The processed variant data structure to search in
-- @param name	description
-- @param required_tags	description
-- @param forbidden_tags	description
-- @param preferred_tags	description
-- @param unwanted_tags	description
-- @return The variant object
function api_methods:find_variant( cooked_methods, name, required_tags, forbidden_tags, preferred_tags, unwanted_tags )
	-- required_tags can be modified if `name` matches below
	required_tags  = required_tags  or {}
	forbidden_tags = forbidden_tags or empty_tbl
	preferred_tags = preferred_tags or empty_tbl
	unwanted_tags  = unwanted_tags  or empty_tbl

	local n_required  = #required_tags
	local n_forbidden = #forbidden_tags

	local _, n_tags, tags
	if string_match( name, "^[%w_]+_v%d*_?%w-[%w_]-$" ) then
		name, _, n_tags, tags = parse_variant( name )

		--print( "find:", name )

		for i = 1, n_tags do
			required_tags[ n_required + i ] = tags[ i ]
		end

		n_required = n_required + n_tags
	end

	if n_required == 0 then
		required_tags[ 1 ] = self.platform
		n_required = 1
	end

	local n = 0
	local matches = {}

	if not cooked_methods[ name ] then
		return nil
	end

	-- Find the minimal subset of matching variants
	for _, obj in pairs( cooked_methods[ name ] ) do
		local found = true

		-- The variants we're looking for must have all the
		-- required tags...
		for i = 1, n_required do
			if not obj[ required_tags[ i ] ] then
				found = false
				break
			end
		end

		-- ...But none of the forbidden tags.
		for i = 1, n_forbidden do
			if obj[ forbidden_tags[ i ] ] then
				found = false
				break
			end
		end

		if found then
			n = n + 1
			matches[ n ] = obj
			-- We can't break here, as more than
			-- one match can be found.
		end
	end

	if n <= 1 then
		-- No variants found or only a single match
		return matches[ 1 ]
	end

	-- None of the above. We have to determine the best candidate.
	local n_preferred = #preferred_tags
	local n_unwanted  = #unwanted_tags

	local best = -1 / 0	-- negative infty
	local n_best
	local best_set

	for i = 1, n do
		local match = matches[ i ]

		-- How suitable is this match. Preferred tags increase,
		-- unwanted tags decrease the value.
		local fitness = 0

		for ii = 1, n_preferred do
			if match[ preferred_tags[ ii ] ] then
				fitness = fitness + 1
			end
		end

		for ii = 1, n_unwanted do
			if match[  unwanted_tags[ ii ] ] then
				fitness = fitness - 1
			end
		end

		if fitness > best then
			-- New best candidate
			best = fitness
			n_best = 1
			best_set = { match }

		elseif fitness == best then
			-- Add to the list
			n_best = n_best + 1
			best_set[ n_best ] = match
		end
	end

	if n_best ~= 1 then
		-- There is more than one candidate, sort the array.
		-- Candidates with a lower ID should go first.
		table_sort( best_set, function( a, b )
			return a._ID < b._ID
		end )
	end

	return best_set[ 1 ]
end

--- Cook a variant into an easily searchable data structure.
-- @param self				(Optional) The optional `self` argument, makes colon notation voluntary
-- @param name				The full name of the variant
-- @param fn				The function  of the variant
-- @param cooked_methods	The table to populate with variant objects
--                      	(method_name->{UUID->{_name=.;_UUID=.;_ID=.;_fn=.}})
-- @return nil
function api_methods.cook_variant( self, name, fn, cooked_methods )
	if type( self ) ~= "table" then
		name, fn, cooked_methods = self, name, fn
	end

	local method, ID, n, tags = parse_variant( name or error( "Expected string for 'name'", 2 ) )
	local method_variants = cooked_methods[ method ] or {}

	local method_obj = {
		-- Almost complete. We'll have to add _UUID later.
		_name = name;
		_ID = ID;
		_fn = fn or error( "Expected function for 'fn'", 2 );
	}

	-- Check for any present platform tags.
	-- If none are found, "anyarch" is implicit.
	local found = false
	for i = 1, n do
		if platform_tags[ tags[ i ] ] then
			found = true
			break
		end
	end

	if not found then
		n = n + 1
		tags[ n ] = "anyarch"
	end

	-- Insert the tags into the variant object
	-- and resolve aliases. Also generate the
	-- UUID along the way.
	local UUID = "v" .. ID
	for i = 1, n do
		local tag = tags[ i ]
		local aliases = alias_tags[ tag ]

		if aliases then
			-- Apply the alias penalty to account
			-- for generic, cross-platform implementations.
			method_obj._ID = method_obj._ID + aliases.penalty or 0

			for ii = 1, #aliases do
				local t = aliases[ ii ]

				method_obj[ t ] = true
				UUID = UUID .. "_" .. t
			end
		else
			method_obj[ tag ] = true
			UUID = UUID .. "_" .. tag
		end
	end

	method_obj.     _UUID   = UUID
	method_variants[ UUID ] = method_obj

	cooked_methods[ method ] = method_variants
end

--- Transforms the API functions and methods to the desired case.
-- @param case	(Optional) The text case to use. Can be "snake", "title", or "camel", case-insensitive.
--            	Also, any non-letter characters and occurences of "case" are removed before checking.
--            	Furthemore, the argument can also be a function in the form of (str)->str, directly
--            	used for the conversion of any text. Defaults to "snake"
-- @return The function used for the conversion
function api_methods:cookapi( case )
	if type( self ) ~= "table" then
		error( "'self' is not a table! Are you using colon notation (`foo:bar()`)?", 2 )
	end

	case = case or "snake"

	local kind = type( case )
	local fn

	if kind == "string" then
		fn = confunctions[ case:lower():gsub( "[%L]+", "" ):gsub( "case", "" ) ]

		if not fn then
			error( "Unknown case '" .. case .. "'", 2 )
		end

	elseif kind == "function" then
		fn = case
	else
		error( "Expected string or function for 'case', got " .. kind, 2 )
	end

	self.convert_function = fn

	for _, lib in pairs( self.loaded ) do
		lib.cook_API( fn )
	end

	return fn
end

--- Load a Desktox module.
-- @param name	The module (same format as for `require()`)
-- @return The loaded module
function api_methods:load  ( name )
	local lib = self.loaded[ name ]

	if not lib then
		lib = dofile( name ) --require( name )

		lib._processed = lib._processed or {}

		-- Cook all variants of the newly loaded module
		for key, fn in pairs( lib.methods ) do
			self:cook_variant( key, fn, lib.cooked_methods )
		end
		for key, fn in pairs( lib ) do
			if string_match( key, "^[%w_]+_v%d*_?%w-[%w_]-$" ) then
				self:cook_variant( key, fn, lib.cooked_lib_methods )
			end
		end

		--- Import new methods into an existing object.
		-- @param object	self
		-- @param methods	The table of methods to import, {method_name->fn}
		-- @return object
		function lib.methods.import( object, methods )
			--[[
			-- DEBUG
			local f = io.open( "/symlink/desktox/dump.txt", "a" )

			for k in next, methods, nil do
				f:write( k .. "\n" )
				f:flush()
			end

			f:write( "\n" )
			f:close()
			--]]

			if not lib._processed[ tostring( methods ) ] then

			for key, fn in pairs( methods ) do
				if type( fn ) == "function" then
					--print( "import:", key )
					self:cook_variant( key, fn, lib.cooked_methods )
				end
			end
				--print( "Process" )
				local n_keys = 0
				local keys = {}

				local conv_fn = self.convert_function

				-- Get all the keys
				for key, v in pairs( methods ) do
					if type( v ) == "function" then
						n_keys = n_keys + 1
						keys[ n_keys ] = key
					end
				end

				-- Convert the keys
				for ii = 1, n_keys do
					local key = keys[ ii ]
					local method_name, variant = string_match( key, "^(.*)(_v%d*_?%w*[%w_]-)$" )

					if method_name then
						local new = conv_fn( method_name )

						if not methods[ new ] then
							methods[ new ] = self:find_variant( lib.cooked_methods, method_name, { self.platform } )._fn
						end

						methods[ new .. variant ] = methods[ key ]
					end
				end

				lib._processed[ tostring( methods ) ] = true
			end

			return object
		end

		--- The __index metamethod for objects of loaded libraries.
		-- Searches for matching variants.
		function lib.meta.__index( obj, key )
			local variant = self:find_variant( lib.cooked_methods, key, { self.platform } )

			if variant then
				local fn = variant._fn

				obj[ key ] = fn
				return fn
			end
		end

		--- The __index metamethod for the loaded libraries themselves.
		-- Searches for matching variants.
		function lib.lib_meta.__index( obj, key )
			if lib.cooked_lib_methods[ key ] then
				local fn = self:find_variant( lib.cooked_lib_methods, key, { self.platform } )._fn

				obj[ key ] = fn
				return fn
			end
		end

		if  lib.set_aliases then
			lib.set_aliases()
		end

		lib.cook_API( self.convert_function, function( cooked, method )
			return self:find_variant( cooked, method, { self.platform } )
		end )

		self.loaded[ name ] = lib
	end

	return lib
end

--- Create a new API instance.
-- @param case	(Optional) The `case` argument to pass to the new instance's :cookapi()
-- @see api_methods.cookapi
-- @return The instance itself
function desktox.new( case )
	local n = setmetatable( {}, api_metatable )

	-- The platform we're running on
	local platform

	if     _G and _G.terralib then
		platform  = "terra"

	elseif _G and _G.jit then
		local  jit_version = _G.jit.version:sub( 1, 10 )

		if     jit_version == "LuaJIT 2.0" then
			platform = "jit20"
		elseif jit_version == "LuaJIT 2.1" then
			platform = "jit21"
		else
			platform = "lua51"
		end

	elseif _VERSION == "Lua 5.1" then
		platform = "lua51"

	elseif _VERSION == "Lua 5.2" then
		platform = "lua52"

	elseif _VERSION == "Lua 5.3" then
		platform = "lua53"
	end

	n.platform = platform

	-- Loaded modules
	n.loaded = {}

	local confunc = api_methods.cookapi( n, case )
	n.convert_function = confunc

	-- Import all known methods
	for k, fn in pairs( api_methods ) do
		n[ k ] = fn
		n[ confunc( k ) ] = fn
	end

	return n
end

--- Provides a case-insensitive memoizing proxy to API functions.
-- @param key	The requested key
-- @return See the description
function api_metatable.__index( _, key )
	local normalised = key:lower():gsub( "[%L]+", "" )

	if normalised ~= key then
		local fn = api_methods[ normalised ]

		if fn then
			-- Memoize to avoid further invocations
			api_methods[ normalised ] = fn
		end

		return fn
	end
end

desktox = setmetatable( desktox, {
	__call =   function( _, ... )
		return desktox.new( ... )
	end;
} )

return desktox
