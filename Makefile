#!/bin/bash

SOURCES    := $(shell find . -path ./battleground -prune -o -type f -name "*.lua" -print)
TESTS      := $(shell find . -path ./battleground -prune -o -type f -name "test_*.lua" -print)
BENCHMARKS := $(shell find . -path ./battleground -prune -o -type f -name "benchmark_*.lua" -path \*/tests/* -print)
FOREIGN    := $(shell find . -path ./battleground -prune -o -type f -name "benchmark_*.lua" -path \*/foreign_tests/* -print)

.PHONY: all setup compile lint test benchmark benchmark-foreign

all: compile lint test benchmark benchmark-foreign

setup:
	@echo -e "\033[1mSetting up environment...\033[0m"
	wget https://raw.githubusercontent.com/mpeterv/hererocks/latest/hererocks.py -O hr.py
	@mkdir -p battleground

compile:
	@# Smoke testing for broken syntax
	@echo -e "\033[43;1;30mCompiling\033[0m"
	@echo -e "luac5.1"
	@luac5.1 -p ${SOURCES}
	@echo -e "luac5.2"
	@luac5.2 -p ${SOURCES}
	@echo -e "luac5.3"
	@luac5.3 -p ${SOURCES}
	@echo -e "luajit"
	@for file in ${SOURCES};\
	do\
		luajit -bl $$file /dev/null;\
	done
	@echo -e "\033[42;30mDone\033[0m"

lint:
	@echo -e "\033[43;1;30mLinting\033[0m"
	luacheck ${SOURCES}

test: setup
	@echo -e "\033[43;1;30mTesting\033[0m\n"
	@for file in ${TESTS};\
	do\
		echo -e -n "\033[35;1mModule ";\
		echo -e -n $$file | cut -d "/" -f2;\
		echo -e "Test   $$file\033[0m\n";\
		\
		echo -e "\033[30;46mlua5.1\033[0m";\
		lua5.1 -l lfs -l require $$file;\
		\
		echo -e "\033[30;46mlua5.2\033[0m";\
		lua5.2 -l lfs -l require $$file;\
		\
		echo -e "\033[30;46mlua5.3\033[0m";\
		lua5.3 -l require $$file;\
		\
		echo -e "\033[30;46mluajit\033[0m";\
		luajit -l lfs -l require $$file;\
	done
	@echo -e "\033[42;30mDone\033[0m"

benchmark: setup
	@echo -e "\033[43;1;30mBenchmarking\033[0m\n"

	@luajit run_benchmarks.lua ${BENCHMARKS}

	@echo -e "\033[42;30mDone\033[0m"

benchmark-foreign: setup
	@echo -e "\033[43;1;30mBenchmarking foreign approaches\033[0m\n"

	@luajit run_foreign_benchmarks.lua ${FOREIGN}

	@echo -e "\033[42;30mDone\033[0m"
