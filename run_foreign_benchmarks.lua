#!/usr/bin/luajit

local cjson = require( "cjson" )
local utils = dofile( "benchmarking_utils.lua" )

local scripts = { ... }
local n_scripts = #scripts
local platform_directory = "battleground"

local platforms   = utils.platforms
local n_platforms = utils.n_platforms

local results = { meta = utils.meta }
local n_results = 0

local code = os.execute [[
	cd battleground;

	if [ -d Surface2 ]; then
		# Pull Surface2
		cd Surface2;
		git pull
	else
		# Clone Surface2
		rm -rf Surface2;
		git clone https://github.com/CrazedProgrammer/Surface2.git;
		cd Surface2;
	fi

	mkdir -p target;

	# Download carl
	wget https://raw.githubusercontent.com/CrazedProgrammer/carl/master/carl.lua -O carl.lua;

	# Build Surface2
	luajit carl.lua build;
]]

if code == 0 then
	os.execute( "echo 'Surface2 built successfully.';" )
else
	error( "Failed to build Surface2!" )
end

-- Benchmark on all supported platforms
for ii = 1, n_platforms do
	local platform = platforms[ ii ]
	local name = platform.name

	for i = 1, n_scripts do
		local path = scripts[ i ]

		print( "\27[35;1mModule  ", ( path:gsub( "^%./", "" ):gsub( "/foreign_tests/.*$", "" ):gsub( "/", "." ) ) )
		print( "Benchmark", path .. "\27[0m" )

		if not platform.optional or platform.optional() then
			local env = platform_directory .. "/" .. name

			print( "\27[30;46m" .. name .. "\27[0m" )

			if not platform.configured then
				-- Setting the 'configured' flag in advance
				-- allows the setup routine to override it.
				-- Any errors are propagated anyway, so
				-- it's safe to do so.
				platform.configured = true

				os.execute( "mkdir -p " .. env )

				if platform.setup then
					print( "Initial platform setup in progress..." )
					platform.setup( env )

					print( "\n" .. name .. " has been initialised successfully.\n" )
				end

				if  platform.info then
					print( name .. " setup information:" )
					platform.info( env )
				end
			end

			local status, data = platform.run( path, env )
			if status ~= 0 then
				error( "Error while benchmarking " .. path .. " on " .. name .. "!", 1 )
			end

			n_results = n_results + 1
			results[ n_results ] = {
				platform = name;
				path = path;
				data = data;
			}
		end
	end
end

-- Prettify the results table
for i = 1, n_results do
	local result = results[ i ]
	results[ result.path ] = results[ result.path ] or {}
	results[ result.path ][ result.platform ] = result.data

	results[ i ] = nil
end

results.meta.n_platforms = n_platforms
results.meta.total_runs = n_results
results.meta.n_scripts = n_scripts

local json_output = cjson.encode( results )

local out = io.open( "benchmark_results_f.json", "w" )
out:write( json_output )
out:close()
