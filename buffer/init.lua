
-- luacheck:globals colours table.unpack

-- Desktox
--  A set of graphics libraries for ComputerCraft by @viluon <viluon@espiv.net>

--   desktox.buffer
--    The Desktox framebuffer library

--    This Source Code Form is subject to the terms of the Mozilla Public
--    License, v. 2.0. If a copy of the MPL was not distributed with this
--    file, You can obtain one at http://mozilla.org/MPL/2.0/.

--    Certain parts of this code were adapted from
--    CrazedProgrammer's Surface2, licensed under the MIT License.
--    Many thanks!

-- In case you'd forget, pixels are stored as
-- `background_colour * PIXEL_VALUE_OFFSET + foreground_colour + character:byte() / PIXEL_VALUE_OFFSET`

local lib_metatable = {}
local buffer = setmetatable( {}, lib_metatable )

local cooked_lib_methods = {}
local cooked_methods = {}
local buffer_methods = {}
local buffer_metatable = {}

-- Constants
local PIXEL_VALUE_OFFSET = 256
local MAX_UNPACK_SIZE = 7996

local TRANSPARENT_VALUES = 254

local TRANSPARENT_BACKGROUND = 254
local TRANSPARENT_FOREGROUND = 255
local TRANSPARENT_CHARACTER  = 0

local TRANSPARENT_PIXEL = TRANSPARENT_BACKGROUND * PIXEL_VALUE_OFFSET
                        + TRANSPARENT_FOREGROUND
                        + TRANSPARENT_CHARACTER

local DEFAULT_BACKGROUND = 48
local DEFAULT_FOREGROUND = 102
local DEFAULT_CHARACTER  = ' '

local DEFAULT_CHARACTER_CODE = DEFAULT_CHARACTER:byte()

-- Utilities
local setmetatable = setmetatable
local tonumber = tonumber
local unpack = table.unpack or unpack
local pairs = pairs
local error = error
local type = type

local string = string
local table = table
local io = io

local remove = table.remove
local insert = table.insert

local sub  = string.sub
local gsub = string.gsub
local match = string.match
local lower = string.lower
local string_char = string.char
local string_byte = string.byte

-- Palettes and colours
local  ANSI_palette1, ANSI_palette2, ANSI_palette3 = {}, {}, {}

local _ANSI_palette = {
	[ 48  ] = "254";	-- white
	[ 49  ] = "214";	-- orange
	[ 50  ] = "206";	-- magenta
	[ 51  ] = "110";	-- lightBlue
	[ 52  ] = "222";	-- yellow
	[ 53  ] = "112";	-- lime
	[ 54  ] = "218";	-- pink
	[ 55  ] = "238";	-- grey
	[ 56  ] = "247";	-- lightGrey
	[ 57  ] = "037";	-- cyan
	[ 97  ] = "134";	-- purple
	[ 98  ] = "026";	-- blue
	[ 99  ] = "094";	-- brown
	[ 100 ] = "070";	-- green
	[ 101 ] = "160";	-- red
	[ 102 ] = "000";	-- black
}

for k, str in pairs( _ANSI_palette ) do
	ANSI_palette1[ k ], ANSI_palette2[ k ], ANSI_palette3[ k ] = string_byte( str, 1, 3 )
end

-- Free the now useless table
_ANSI_palette = nil -- luacheck:ignore

local colours   = setmetatable( {
	white		=  48;
	orange		=  49;
	magenta		=  50;
	lightblue	=  51;
	yellow		=  52;
	lime		=  53;
	pink		=  54;
	grey		=  55;
	lightgrey	=  56;
	cyan		=  57;
	purple		=  97;
	blue		=  98;
	brown		=  99;
	green		= 100;
	red			= 101;
	black		= 102;

	transparentbackground = TRANSPARENT_BACKGROUND;
	transparentforeground = TRANSPARENT_FOREGROUND;

	-- Aliases
	transparentbg = TRANSPARENT_BACKGROUND;
	transparentfg = TRANSPARENT_FOREGROUND;
}, {
	--- A memoizing "fuzzy" __index.
	__index = function( tbl, k )
		local normalised_key = gsub( lower( k ), "%L", "" )

		if k ~= normalised_key then
			local value = tbl[ normalised_key ]
			tbl[ k ] = value

			return value
		end
	end;
} )

-- An array of known colours
local colour_array = {
	colours.white,
	colours.orange,
	colours.magenta,
	colours.lightBlue,
	colours.yellow,
	colours.lime,
	colours.pink,
	colours.grey,
	colours.lightGrey,
	colours.cyan,
	colours.purple,
	colours.blue,
	colours.brown,
	colours.green,
	colours.red,
	colours.black,
	TRANSPARENT_BACKGROUND,
	TRANSPARENT_FOREGROUND,
}

-- Generate a bidirectional lookup table
local colour_lookup = {}
for i = 1, #colour_array do
	local colour = colour_array[ i ]
	local hex = sub( "0123456789abcdefgh", i, i )

	colour_lookup[ colour ] = hex
	colour_lookup[ hex ] = colour
end

local byte = setmetatable( {}, {
	__index = function( t, k )
		local b = string_byte( k ) / PIXEL_VALUE_OFFSET
		t[ k ] = b
		return b
	end;
} )

--[[
-- @todo remove!!
local char = setmetatable( {}, {
	__index = function( t, k )
		local b = string_char( k )
		t[ k ] = b
		return b
	end;
} )
--]]

-- Error messages
local unable_to_set_optional_argument = "Unable to set optional argument "
local expected_number_for = "Expected number for "

-- Utility functions

--- Round a number to a set amount of decimal places.
-- @param n			The number to round
-- @param places	(Optional) The amount of decimal places to keep, defaults to 0
-- @return The rounded number
local function round( n, places )
	local mult = 10 ^ ( places or 0 )
	local m = n * mult + 0.5

	return ( m - m % 1 ) / mult
end

--- Resize the buffer.
-- @param width				(Optional) The desired new width, defaults to self.width
-- @param height			(Optional) The desired new height, defaults to self.height
-- @param background_colour	(Optional) The background colour to set any new pixels to, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to set any new pixels to, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to set any new pixels to, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:resize_v_dtt( width, height, background_colour, foreground_colour, character )
	-- @todo Shouldn't use insert/remove
	local self_width = self.width
	local self_height = self.height
	local n_self = self.length

	width = width or self_width
	height = height or self_height

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	-- Loop through all lines
	for y = ( self_height < height and self_height or height ) - 1, 0, -1 do
		if width > self_width then
			local line_offset = y * self_width

			-- Insert pixels at the end of the line
			for _ = 0, width - self_width - 1 do
				insert( self, line_offset, new_pixel )
				n_self = n_self + 1
			end

		elseif width < self_width then
			local line_offset = y * width

			-- Drop the pixels exceeding new width
			for _ = 0, self_width - width - 1 do
				remove( self, line_offset )
				n_self = n_self - 1
			end
		end
	end

	if height > self_height then
		-- Insert blank lines
		for i = n_self, width * height - 1 do
			self[ i ] = new_pixel
		end

	elseif height < self_height then
		-- Drop extra lines
		for i = width * height - 1, n_self do
			self[ i ] = nil
		end
	end

	self.width = width
	self.height = height
	self.length = width * height - 1

	self.x2 = self.x1 + width  - 1
	self.y2 = self.y1 + height - 1

	return self
end

--- Map the buffer with the given function.
-- @param fn		The function to map with, given arguments self, x, y, pixel
-- @param start_x	(Optional) The x coordinate in self to start mapping from, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate in self to start mapping from, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate in self to stop mapping at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate in self to stop mapping at, 0-based, defaults to self.height - 1
-- @return self
function buffer_methods:map_v( fn, start_x, start_y, end_x, end_y )
	if type( fn ) ~= "function" then
		error( "Expected function for 'fn'", 2 )
	end

	local almost_w = self.width  - 1
	local almost_h = self.height - 1

	-- Avoid function calls (max()) where possible
	if  start_x then
		start_x = start_x < 0 and 0 or start_x
	else
		start_x = 0
	end

	if  end_x then
		end_x = end_x > almost_w and almost_w or end_x
	else
		end_x = almost_w
	end

	if end_x < start_x then
		start_x, end_x = end_x, start_x
	end

	if  start_y then
		start_y = start_y < 0 and 0 or start_y
	else
		start_y = 0
	end

	if  end_y then
		end_y = end_y > almost_h and almost_h or end_y
	else
		end_y = almost_h
	end

	if end_y < start_y then
		start_y, end_y = end_y, start_y
	end

	local w = almost_w + 1

	-- Loop through all pixels
	for y = start_y, end_y do
		local line_offset = y * w

		for x = start_x, end_x do
			self[ line_offset + x ] = fn( self, x, y, self[ line_offset + x ] )
		end
	end

	return self
end

--- Map the buffer with the given palette, replacing colours.
-- @param palette	The palette to map with (colour->colour)
-- @param start_x	(Optional) The x coordinate in self to start mapping from, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate in self to start mapping from, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate in self to stop mapping at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate in self to stop mapping at, 0-based, defaults to self.height - 1
-- @return self, old_self	Self and the self state before mapping
function buffer_methods:shade_v( palette, start_x, start_y, end_x, end_y )
	if type( palette ) ~= "table" then
		error( "Expected table for 'palette'", 2 )
	end

	local w = self.width
	local almost_w = w - 1
	local almost_h = self.height - 1

	-- Avoid function calls where possible
	if  start_x then
		start_x = start_x < 0 and 0 or start_x
	else
		start_x = 0
	end

	if  end_x then
		end_x = end_x > almost_w and almost_w or end_x
	else
		end_x = almost_w
	end

	if end_x < start_x then
		start_x, end_x = end_x, start_x
	end

	if  start_y then
		start_y = start_y < 0 and 0 or start_y
	else
		start_y = 0
	end

	if  end_y then
		end_y = end_y > almost_h and almost_h or end_y
	else
		end_y = almost_h
	end

	if end_y < start_y then
		start_y, end_y = end_y, start_y
	end

	-- Loop through all pixels
	for y = start_y, end_y do
		local line_offset = y * w

		for x = start_x, end_x do
			local pixel = self[ line_offset + x ]

			local background_colour = pixel /     PIXEL_VALUE_OFFSET
			local foreground_colour = pixel %     PIXEL_VALUE_OFFSET
			local character         = pixel % 1 * PIXEL_VALUE_OFFSET

			self[ line_offset + x ] = ( palette[ background_colour ] or background_colour ) * PIXEL_VALUE_OFFSET
			                        + ( palette[ foreground_colour ] or foreground_colour )
			                        + byte[ character ]
		end
	end

	return self
end

--- Clone the buffer into a new object.
-- @param data		(Optional) The data of the new clone (a regular array of pixels), pulls from self by default
-- @return buffer	The new buffer, a clone of self
function buffer_methods:clone_v( data )
	local _clone = {}

	-- Clone the pixel data
	if data and type( data ) == "table" then
		for i = 1, #data do
			_clone[ i - 1 ] = data[ i ]
		end
	else
		for i = 0, self.length do
			_clone[ i ] = self[ i ]
		end
	end

	-- @todo Localise .new_from_points
	return buffer.new_from_points( self.x1, self.y1, self.x2, self.y2, self.parent, nil, nil, nil, true, _clone )
end

--- Render the buffer to another buffer.
-- @param target				(Optional) The buffer to render to, defaults to self.parent
-- @param x						(Optional) The x coordinate in target to render self at, 0-based, defaults to self.x1
-- @param y						(Optional) The y coordinate in target to render self at, 0-based, defaults to self.y1
-- @param start_x				(Optional) The x coordinate in self to start rendering from, 0-based, defaults to 0
-- @param start_y				(Optional) The y coordinate in self to start rendering from, 0-based, defaults to 0
-- @param end_x					(Optional) The x coordinate in self to stop rendering at, 0-based, defaults to self.width - 1
-- @param end_y					(Optional) The y coordinate in self to stop rendering at, 0-based, defaults to self.height - 1
-- @param transparency_target	(Optional) The buffer to use for transparency resolution, defaults to target
-- @return self
function buffer_methods:render_v_rtt( target, x, y, start_x, start_y, end_x, end_y, transparency_target )
	target = target or self.parent or error( unable_to_set_optional_argument .. "'target': self.parent is nil", 2 )
	x      = x      or self.x1     or error( unable_to_set_optional_argument .. "'x': self.x1 is nil", 2 )
	y      = y      or self.y1     or error( unable_to_set_optional_argument .. "'y': self.y1 is nil", 2 )

	transparency_target = transparency_target or target

	local self_width = self.width
	local target_width = target.width

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or self_width  - 1
	end_y = end_y or self.height - 1

	-- Respect borders of the render target
	start_x = start_x > -x and start_x or -x
	start_y = start_y > -y and start_y or -y
	end_x   = ( end_x < ( target_width  - x - 1 ) and end_x or target_width  - x - 1 )
	end_y   = ( end_y < ( target.height - y - 1 ) and end_y or target.height - y - 1 )

	-- Will be explained further down
	local false_parent = { parent = transparency_target }

	-- Loop through all coordinates
	for _y = start_y, end_y do
		-- Cache the line position for both the target buffer and us
		local target_offset = ( _y + y ) * target_width + x
		local local_offset  = _y * self_width

		for _x = start_x, end_x do
			-- index is where we should store the resultant pixel in target
			local index = target_offset + _x
			-- pixel is the data from our buffer
			local pixel = self[ local_offset + _x ]

			-- We actually don't have to draw anything if the pixel is fully transparent - it has no effect on the parent
			if pixel ~= TRANSPARENT_PIXEL or target ~= transparency_target then
				-- Pixel breakdown!
				local bg = pixel /     PIXEL_VALUE_OFFSET
				local fg = pixel %     PIXEL_VALUE_OFFSET
				local ch = pixel % 1 * PIXEL_VALUE_OFFSET

				-- Set the pixel in target, resolving transparency along the way
				if bg >= TRANSPARENT_VALUES or fg >= TRANSPARENT_VALUES or ch == TRANSPARENT_CHARACTER then
					local local_parent = false_parent

					local background_colour = bg
					local foreground_colour = fg
					local character = ch

					local tracked_offset_x = x
					local tracked_offset_y = y

					-- Not only we have to get to the non-transparent colour
					-- when the background_colour is transparent, we also have
					-- to resolve the colour if the *foreground* uses it

					if background_colour >= TRANSPARENT_VALUES or fg >= TRANSPARENT_BACKGROUND then
						local tracked_colour = TRANSPARENT_BACKGROUND

						-- Down into the rabbit hole we go. One parent level further with every iteration.
						-- We have to let the loop run at least once, for the TRANSPARENT_BACKGROUND to resolve
						repeat
							-- This wouldn't work the first time (when the loop starts), that's
							-- why we defined local_parent to be the false_parent
							local_parent = local_parent.parent

							if not local_parent then
								-- We've reached the very bottom of the family tree, without luck
								background_colour = DEFAULT_BACKGROUND
								break
							end

							-- All buffers have a position, we have to keep track of that
							tracked_offset_x = tracked_offset_x + local_parent.x1
							tracked_offset_y = tracked_offset_y + local_parent.y1

							local actual_y = _y + tracked_offset_y
							local actual_x = _x + tracked_offset_x
							local p_width  = local_parent.width
							local p_height = local_parent.height

							-- Check that we are within bounds
							-- We could subtract a 1 in the local definition above, but if actual_x < 0 then the operation
							-- doesn't even happen :P
							if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
								-- We can't get a pixel out of bounds of this parent, so we'll just roll with
								-- the default colour.
								-- @todo Would it be better if we'd look for any parent that *is* within bounds instead?
								--       Probably not, the result isn't visible anyway since that part of self is outside
								--       of the parent
								background_colour = DEFAULT_BACKGROUND
								break
							end

							local parent_pixel = local_parent[ actual_y * p_width + actual_x ]

							if tracked_colour == TRANSPARENT_BACKGROUND then
								background_colour = parent_pixel / PIXEL_VALUE_OFFSET
							else
								background_colour = parent_pixel % PIXEL_VALUE_OFFSET
							end

							tracked_colour = background_colour - background_colour % 1

						until background_colour < TRANSPARENT_VALUES
					end

					-- The same goes for the foreground colour...
					if foreground_colour >= TRANSPARENT_VALUES or ( bg < TRANSPARENT_BACKGROUND and bg >= TRANSPARENT_FOREGROUND ) then

						local_parent = false_parent
						tracked_offset_x = x
						tracked_offset_y = y

						local tracked_colour = TRANSPARENT_FOREGROUND

						repeat
							local_parent = local_parent.parent

							if not local_parent then
								foreground_colour = DEFAULT_FOREGROUND
								break
							end

							tracked_offset_x = tracked_offset_x + local_parent.x1
							tracked_offset_y = tracked_offset_y + local_parent.y1

							local actual_y = _y + tracked_offset_y
							local actual_x = _x + tracked_offset_x
							local p_width  = local_parent.width
							local p_height = local_parent.height

							if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
								foreground_colour = DEFAULT_FOREGROUND
								break
							end

							local parent_pixel = local_parent[ actual_y * p_width + actual_x ]

							if tracked_colour == TRANSPARENT_FOREGROUND then
								foreground_colour = parent_pixel % PIXEL_VALUE_OFFSET
							else
								foreground_colour = parent_pixel / PIXEL_VALUE_OFFSET
							end

							tracked_colour = foreground_colour - foreground_colour % 1

						until foreground_colour < TRANSPARENT_VALUES
					end

					-- ...And finally for the character.
					-- Note that a colour cannot be transparent to a character, so
					-- there's no extra branch here
					local_parent = false_parent
					tracked_offset_x = x
					tracked_offset_y = y

					while character == TRANSPARENT_CHARACTER do
						local_parent = local_parent.parent

						if not local_parent then
							character = DEFAULT_CHARACTER_CODE
							break
						end

						tracked_offset_x = tracked_offset_x + local_parent.x1
						tracked_offset_y = tracked_offset_y + local_parent.y1

						local actual_y = _y + tracked_offset_y
						local actual_x = _x + tracked_offset_x
						local p_width = local_parent.width
						local p_height = local_parent.height

						if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
							character = DEFAULT_CHARACTER_CODE
							break
						end

						character = local_parent[ actual_y * p_width + actual_x ] % 1 * PIXEL_VALUE_OFFSET
					end

					-- Assign the proper data to the rendered pixel
					-- @todo Rewrite as if-else blocks and simplify
					bg = bg - bg % 1
					fg = fg - fg % 1
					background_colour = background_colour - background_colour % 1
					foreground_colour = foreground_colour - foreground_colour % 1

					if bg == TRANSPARENT_BACKGROUND then
						bg = background_colour
					elseif bg == TRANSPARENT_FOREGROUND then
						bg = foreground_colour
					end

					if fg == TRANSPARENT_BACKGROUND then
						fg = background_colour
					elseif fg == TRANSPARENT_FOREGROUND then
						fg = foreground_colour
					end

					if ch == TRANSPARENT_CHARACTER then
						ch = character
					end

					target[ index ] = bg * PIXEL_VALUE_OFFSET + fg + ch / PIXEL_VALUE_OFFSET
				else
					-- That was quick! This is a speed up for the large number of cases
					-- in which no transparency is used
					target[ index ] = pixel
				end
			end
		end
	end

	return self
end

--- Render the buffer to another buffer.
-- @param target				(Optional) The buffer to render to, defaults to self.parent
-- @param x						(Optional) The x coordinate in target to render self at, 0-based, defaults to self.x1
-- @param y						(Optional) The y coordinate in target to render self at, 0-based, defaults to self.y1
-- @param start_x				(Optional) The x coordinate in self to start rendering from, 0-based, defaults to 0
-- @param start_y				(Optional) The y coordinate in self to start rendering from, 0-based, defaults to 0
-- @param end_x					(Optional) The x coordinate in self to stop rendering at, 0-based, defaults to self.width - 1
-- @param end_y					(Optional) The y coordinate in self to stop rendering at, 0-based, defaults to self.height - 1
-- @param transparency_target	(Optional) The buffer to use for transparency resolution, defaults to target
-- @return self
function buffer_methods:render2_v_rtt( target, x, y, start_x, start_y, end_x, end_y, transparency_target )
	target = target or self.parent --or error( unable_to_set_optional_argument .. "'target', self.parent is nil", 2 )
	transparency_target = transparency_target or target

	local w = self.width
	local h = self.height

	local self_x = self.x1
	local self_y = self.y1

	-- This avoids attempts to something something nil while enabling a nil parent
	local target_width, target_height

	if target then
		target_width  = target.width or 0
		target_height = target.height
	end

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or w - 1
	end_y = end_y or h - 1

	if target then
		-- Respect borders of the target
		-- @todo Rewrite with ifs
		start_x = start_x > -self_x and start_x or -self_x
		start_y = start_y > -self_y and start_y or -self_y
		end_x   = ( end_x < ( target_width  - self_x - 1 ) and end_x or target_width  - self_x - 1 )
		end_y   = ( end_y < ( target_height - self_y - 1 ) and end_y or target_height - self_y - 1 )
	end

	-- The last pixel read, whether it is transparent, its colours and character
	local  last_pixel, transparent, bg, fg, ch
	local resolve_bg, resolve_fg, resolve_ch

	-- Keep track of which colour we're redirecting to
	local tracked_bg_colour, tracked_fg_colour

	-- The transparency resolution target which changes as the
	-- algorithm goes through the parent hierarchy. Defining
	-- here and resetting at the end of the branch avoids an
	-- extra (MOVE) instruction for non-transparent pixels.
	-- Yes, I <3 micro-optimisation.
	local current_target = transparency_target

	for _y = start_y, end_y do
		local target_offset = ( _y + y ) * w + x
		local line_offset = _y * w

		for _x = start_x, end_x do
			local pixel = self[ line_offset + _x ]

			local final_bg, final_fg

			if pixel ~= last_pixel then
				bg = pixel /     PIXEL_VALUE_OFFSET
				fg = pixel %     PIXEL_VALUE_OFFSET
				ch = pixel % 1 * PIXEL_VALUE_OFFSET

				last_pixel = pixel

				-- Determine which colours should be resolved (read from
				-- the parent buffer). Also get ready to track redirections.
				resolve_fg, resolve_bg = false, false

				if bg >= TRANSPARENT_FOREGROUND then
					-- Transparent to foreground
					resolve_fg = true
					tracked_bg_colour = TRANSPARENT_FOREGROUND

				elseif bg >= TRANSPARENT_BACKGROUND then
					-- Transparent to background
					resolve_bg = true
					tracked_bg_colour = TRANSPARENT_BACKGROUND
				end

				if fg >= TRANSPARENT_FOREGROUND then
					-- Transparent to foreground
					resolve_fg = true
					tracked_fg_colour = TRANSPARENT_FOREGROUND

				elseif fg >= TRANSPARENT_BACKGROUND then
					-- Transparent to background
					resolve_bg = true
					tracked_fg_colour = TRANSPARENT_BACKGROUND
				end

				resolve_ch = ch == TRANSPARENT_CHARACTER

				transparent = resolve_bg or resolve_fg or resolve_ch
			end

			-- Resolve transparency!!
			if transparent then
				local res_bg, res_fg, res_ch = resolve_bg, resolve_fg, resolve_ch
				local _bg, _fg, _ch = bg, fg, ch

				local tracked_fg, tracked_bg = tracked_fg_colour, tracked_bg_colour

				local tracked_offset_x = self_x
				local tracked_offset_y = self_y

				while true do
					-- Resolve flags for a future pass
					local fg_needs_res_bg, bg_needs_res_fg

					tracked_offset_x = tracked_offset_x + current_target.x1
					tracked_offset_y = tracked_offset_y + current_target.y1

					local actual_x = x + tracked_offset_x
					local actual_y = y + tracked_offset_y

					local _width = current_target.width

					if  actual_y >= 0 and actual_y < current_target.height
					and actual_x >= 0 and actual_x < _width then
						local underneath = current_target[ actual_y * _width + actual_x ]

						if res_bg then
							_bg = underneath / PIXEL_VALUE_OFFSET

							if _bg >= TRANSPARENT_FOREGROUND then
								bg_needs_res_fg = true
								tracked_bg = TRANSPARENT_FOREGROUND

							elseif _bg >= TRANSPARENT_BACKGROUND then
								res_bg = true
								tracked_bg = TRANSPARENT_BACKGROUND

							else
								res_bg = false
							end
						end

						if res_fg then
							_fg = underneath % PIXEL_VALUE_OFFSET

							if _fg >= TRANSPARENT_FOREGROUND then
								res_fg = true
								tracked_fg = TRANSPARENT_FOREGROUND

							elseif _fg >= TRANSPARENT_BACKGROUND then
								-- We use an extra flag here in order not to overwrite
								-- the background resolution branch's result
								fg_needs_res_bg = true
								tracked_fg = TRANSPARENT_BACKGROUND

							else
								res_fg = false
							end
						end

						if res_ch then
							_ch = underneath % 1 * PIXEL_VALUE_OFFSET

							res_ch = _ch == TRANSPARENT_CHARACTER
						end

						if not ( res_bg or res_fg or res_ch ) then
							break
						end

						current_target = current_target.parent

						if not current_target then
							if res_bg then
								_bg = DEFAULT_BACKGROUND
							end
							if res_fg then
								_fg = DEFAULT_FOREGROUND
							end
							if res_ch then
								_ch = DEFAULT_CHARACTER_CODE
							end

							break
						end

						res_bg = res_bg or fg_needs_res_bg
						res_fg = res_fg or bg_needs_res_fg

					else
						if res_bg then
							_bg = DEFAULT_BACKGROUND
						end
						if res_fg then
							_fg = DEFAULT_FOREGROUND
						end
						if res_ch then
							_ch = DEFAULT_CHARACTER_CODE
						end

						break
					end
				end

				-- @todo Simplify
				if bg >= TRANSPARENT_VALUES then
					if tracked_bg == TRANSPARENT_BACKGROUND then
						final_bg = _bg
					else
						final_bg = _fg
					end
				else
					final_bg = _bg
				end

				if fg >= TRANSPARENT_VALUES then
					if tracked_fg == TRANSPARENT_FOREGROUND then
						final_fg = _fg
					else
						final_fg = _bg
					end
				else
					final_fg = _fg
				end

				final_bg = final_bg - final_bg % 1
				final_fg = final_fg - final_fg % 1

				target[ target_offset + _x] = final_bg * PIXEL_VALUE_OFFSET
				                            + final_fg
				                            + _ch      / PIXEL_VALUE_OFFSET

				-- Reset for the next transparency resolution pass,
				-- as advertised :P
				current_target = transparency_target

			else
				target[ target_offset + _x ] = pixel
			end
		end
	end

	return self
end

--- Render the buffer to a CraftOS window object.
-- @param target	The window to render to
-- @param x			(Optional) The x coordinate in target to render self at, 1-based, defaults to self.x1 + 1
-- @param y			(Optional) The y coordinate in target to render self at, 1-based, defaults to self.y1 + 1
-- @param visible	(Optional) Whether the terminal object (if it's a window) should be visible after rendering.
--					If set, this option can significantly speed up the rendering process by avoiding window redraws.
-- @param parent	(Optional) The buffer to use for transparency resolution, defaults to self.parent
-- @param start_x	(Optional) The x coordinate to start reading data (from self) at, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate to start reading data (from self) at, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate to end reading data (from self) at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate to end reading data (from self) at, 0-based, defaults to self.height - 1
-- @param palette	(Optional) A lookup table to use for converting colours,
--               	defaults to ComputerCraft's native colour mapping
-- @param dont_fix	(Optional) If true, the supplied palette won't be checked for correctness
--					It is recommended to use valid palettes (ones that contain entries for all known colours) along
--					with this flag, to ensure best performance
-- @return self
function buffer_methods:render_to_window_v_rtt( target, x, y, visible, parent,
                                                start_x, start_y, end_x, end_y, palette, dont_fix )
	-- @todo Rewrite with debugging symbols
	x = x or ( self.x1 and self.x1 + 1 or error( unable_to_set_optional_argument .. "'x': self.x1 is nil", 2 ) )
	y = y or ( self.y1 and self.y1 + 1 or error( unable_to_set_optional_argument .. "'y': self.y1 is nil", 2 ) )

	parent = parent or self.parent

	local setc, blit  = target.setCursorPos, target.blit
	local set_visible = target.setVisible

	if visible ~= nil and set_visible then
		set_visible( false )
	end

	-- Go through all lines of the buffer
	-- @todo Localise :cook_lines
	local lines = self:cook_lines( parent, start_x, start_y, end_x, end_y, palette, dont_fix )

	local line = 0
	for i = 1, #lines, 3 do
		setc( x, y + line )
		blit( lines[ i + 2 ], lines[ i + 1 ], lines[ i ] )

		line = line + 1
	end

	if visible == true and set_visible then
		set_visible( true )
	end

	return self
end

--- Convert buffer data into a format useful for blitting to ComputerCraft terminal objects.
-- @param target	(Optional) The target buffer to use for transparency resolution, defaults to self.parent
-- @param start_x	(Optional) The x coordinate to start reading data at, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate to start reading data at, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate to end reading data at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate to end reading data at, 0-based, defaults to self.height - 1
-- @return	An array of strings, where for i = 1, buf.height, 3 each i-th string contains
--			background colour data, each (i+1)-th string contains foreground colour data
--			and each (i+2)-th string represents the characters
function buffer_methods:cook_lines_v_rtt( target, start_x, start_y, end_x, end_y )
	target = target or self.parent --or error( unable_to_set_optional_argument .. "'target', self.parent is nil", 2 )

	local line = 1
	local lines = {}

	local w = self.width
	local h = self.height

	local self_x = self.x1
	local self_y = self.y1

	-- This avoids attempts to something something nil while enabling a nil parent
	local target_width, target_height

	if target then
		target_width  = target.width or 0
		target_height = target.height
	end

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or w - 1
	end_y = end_y or h - 1

	if target then
		-- Respect borders of the target
		-- @todo Rewrite with ifs
		start_x = start_x > -self_x and start_x or -self_x
		start_y = start_y > -self_y and start_y or -self_y
		end_x   = ( end_x < ( target_width  - self_x - 1 ) and end_x or target_width  - self_x - 1 )
		end_y   = ( end_y < ( target_height - self_y - 1 ) and end_y or target_height - self_y - 1 )
	end

	-- The last pixel read, whether it is transparent, its colours and character
	local  last_pixel, transparent, bg, fg, ch
	local resolve_bg, resolve_fg, resolve_ch

	-- Keep track of which colour we're redirecting to
	local tracked_bg_colour, tracked_fg_colour

	-- The transparency resolution target which changes as the
	-- algorithm goes through the parent hierarchy. Defining
	-- here and resetting at the end of the branch avoids an
	-- extra (MOVE) instruction for non-transparent pixels.
	-- Yes, I <3 micro-optimisation.
	local current_target = target

	for y = start_y, end_y do
		local line_offset = y * w

		local index = 1

		local background = {}
		local foreground = {}
		local characters = {}

		for x = start_x, end_x do
			local pixel = self[ line_offset + x ]

			if pixel ~= last_pixel then
				bg = pixel /     PIXEL_VALUE_OFFSET
				fg = pixel %     PIXEL_VALUE_OFFSET
				ch = pixel % 1 * PIXEL_VALUE_OFFSET

				last_pixel = pixel

				-- Determine which colours should be resolved (read from
				-- the parent buffer). Also get ready to track redirections.
				resolve_fg, resolve_bg = false, false

				if bg >= TRANSPARENT_FOREGROUND then
					-- Transparent to foreground
					resolve_fg = true
					tracked_bg_colour = TRANSPARENT_FOREGROUND

				elseif bg >= TRANSPARENT_BACKGROUND then
					-- Transparent to background
					resolve_bg = true
					tracked_bg_colour = TRANSPARENT_BACKGROUND
				end

				if fg >= TRANSPARENT_FOREGROUND then
					-- Transparent to foreground
					resolve_fg = true
					tracked_fg_colour = TRANSPARENT_FOREGROUND

				elseif fg >= TRANSPARENT_BACKGROUND then
					-- Transparent to background
					resolve_bg = true
					tracked_fg_colour = TRANSPARENT_BACKGROUND
				end

				resolve_ch = ch == TRANSPARENT_CHARACTER

				transparent = resolve_bg or resolve_fg or resolve_ch
			end

			-- Resolve transparency!!
			if transparent then
				local res_bg, res_fg, res_ch = resolve_bg, resolve_fg, resolve_ch
				local _bg, _fg, _ch = bg, fg, ch

				local tracked_fg, tracked_bg = tracked_fg_colour, tracked_bg_colour

				local tracked_offset_x = self_x
				local tracked_offset_y = self_y

				while true do
					-- Resolve flags for a future pass
					local fg_needs_res_bg, bg_needs_res_fg

					tracked_offset_x = tracked_offset_x + current_target.x1
					tracked_offset_y = tracked_offset_y + current_target.y1

					local actual_x = x + tracked_offset_x
					local actual_y = y + tracked_offset_y

					local _width = current_target.width

					if  actual_y >= 0 and actual_y < current_target.height
					and actual_x >= 0 and actual_x < _width then
						local underneath = current_target[ actual_y * _width + actual_x ]

						if res_bg then
							_bg = underneath / PIXEL_VALUE_OFFSET

							if _bg >= TRANSPARENT_FOREGROUND then
								bg_needs_res_fg = true
								tracked_bg = TRANSPARENT_FOREGROUND

							elseif _bg >= TRANSPARENT_BACKGROUND then
								res_bg = true
								tracked_bg = TRANSPARENT_BACKGROUND

							else
								res_bg = false
							end
						end

						if res_fg then
							_fg = underneath % PIXEL_VALUE_OFFSET

							if _fg >= TRANSPARENT_FOREGROUND then
								res_fg = true
								tracked_fg = TRANSPARENT_FOREGROUND

							elseif _fg >= TRANSPARENT_BACKGROUND then
								-- We use an extra flag here in order not to overwrite
								-- the background resolution branch's result
								fg_needs_res_bg = true
								tracked_fg = TRANSPARENT_BACKGROUND

							else
								res_fg = false
							end
						end

						if res_ch then
							_ch = underneath % 1 * PIXEL_VALUE_OFFSET

							res_ch = _ch == TRANSPARENT_CHARACTER
						end

						if not ( res_bg or res_fg or res_ch ) then
							break
						end

						current_target = current_target.parent

						if not current_target then
							if res_bg then
								_bg = DEFAULT_BACKGROUND
							end
							if res_fg then
								_fg = DEFAULT_FOREGROUND
							end
							if res_ch then
								_ch = DEFAULT_CHARACTER
							end

							break
						end

						res_bg = res_bg or fg_needs_res_bg
						res_fg = res_fg or bg_needs_res_fg

					else
						if res_bg then
							_bg = DEFAULT_BACKGROUND
						end
						if res_fg then
							_fg = DEFAULT_FOREGROUND
						end
						if res_ch then
							_ch = DEFAULT_CHARACTER
						end

						break
					end
				end

				if bg >= TRANSPARENT_VALUES then
					if tracked_bg == TRANSPARENT_BACKGROUND then
						background[ index ] = _bg
					else
						background[ index ] = _fg
					end
				else
					background[ index ] = _bg
				end

				if fg >= TRANSPARENT_VALUES then
					if tracked_fg == TRANSPARENT_FOREGROUND then
						foreground[ index ] = _fg
					else
						foreground[ index ] = _bg
					end
				else
					foreground[ index ] = _fg
				end

				characters[ index ] = _ch

				-- Reset for the next transparency resolution pass,
				-- as advertised :P
				current_target = target

			else
				background[ index ] = bg
				foreground[ index ] = fg
				characters[ index ] = ch
			end

			index = index + 1
		end

		lines[ line     ] = string_char( unpack( background ) )
		lines[ line + 1 ] = string_char( unpack( foreground ) )
		lines[ line + 2 ] = string_char( unpack( characters ) )

		line = line + 3
	end

	return lines
end

--- Get a CraftOS window-like interface for interaction with the buffer.
--	The window is a valid term object, which means that the CraftOS term
--	can be term.redirect()ed to it. Functions without a return value,
--	unlike those in the native API, return the window object, allowing for
--	method chaining.
-- @param parent	The parent terminal, such as term.current() or a window
-- @param x			(Optional) The x coordinate of the window object in parent, 1-based, defaults to 1
-- @param y			(Optional) The y coordinate of the window object in parent, 1-based, defaults to 1
-- @param width		(Optional) The width of the window object, defaults to self.width.
--             		If different from self.width, buffer will be resized
-- @param height	(Optional) The height of the window object, defaults to self.height.
--              	If different from self.height, buffer will be resized
-- @param visible	(Optional) Whether the window is visible (i.e. changes are propagated to parent in real time),
--               	defaults to false
-- @param is_colour	(Optional) Whether the window supports colour (will only display monochromatic colours otherwise),
--					defaults to parent.isColour()
-- @return window	CraftOS-like window object
function buffer_methods:get_window_interface_v( parent, x, y, width, height, visible, is_colour )
	-- @todo Make parent optional (win.redirect?) or at least let it be a buffer
	-- @todo Actually define the functions as locals first, then set them in win
	local win = {}

	x = x or 1
	y = y or 1
	width  = width  or self.width
	height = height or self.height
	-- Visibility defaults to false
	visible = visible and true or false

	-- Colour defaults to parent.isColour()
	if is_colour == nil then
		is_colour = parent.isColour and parent.isColour()
	end

	-- Retrieve information from parent
	local cursor_blink = parent.getCursorBlink and parent.getCursorBlink() or false

	local background_colour = parent.getBackgroundColour()
	local text_colour = parent.getTextColour()

	local cursor_x = 1
	local cursor_y = 1

	function win.write( text )
		local    x_offset = cursor_x - 1
		local line_offset = ( cursor_y - 1 ) * width

		for i = 1, #text do
			if x_offset >= width then
				break
			end

			self[ line_offset + x_offset ] = { background_colour, text_colour, sub( text, i, i ) }
			x_offset = x_offset + 1
		end

		cursor_x = x_offset + 1

		if visible then
			return win.redraw()
		end

		return win
	end

	function win.blit( text, text_colours, background_colours )
		self:blit( cursor_x - 1, cursor_y - 1, text, background_colours, text_colours )

		if visible then
			return win.redraw()
		end

		return win
	end

	function win.clear()
		self:clear( background_colour )

		if visible then
			return win.redraw()
		end

		return win
	end

	function win.clearLine()
		self:clear_line( cursor_y - 1 )

		if visible then
			return win.redraw()
		end

		return win
	end

	function win.getCursorPos()
		return cursor_x, cursor_y
	end

	function win.setCursorPos( new_x, new_y )
		new_x = tonumber( new_x ) or error( expected_number_for .. "'new_x'", 2 )
		new_y = tonumber( new_y ) or error( expected_number_for .. "'new_y'", 2 )

		cursor_x, cursor_y = new_x, new_y
	end

	function win.setCursorBlink( bool )
		cursor_blink = bool

		return win
	end

	function win.isColor()
		return is_colour
	end

	function win.getSize()
		return width, height
	end

	function win.scroll( n )
		-- @todo Localise this
		self:shift( 0, n, background_colour )

		if visible then
			return win.redraw()
		end

		return win
	end

	function win.setTextColor( colour )
		text_colour = colour
		return win
	end

	function win.getTextColor()
		return text_colour
	end

	function win.setBackgroundColor( colour )
		background_colour = colour
		return win
	end

	function win.getBackgroundColor()
		return background_colour
	end

	function win.setTextScale( _ )
		-- Scale is ignored for now
		return win
	end

	function win.setVisible( visibility )
		if visible ~= visibility then
			visible = visibility

			if visible then
				return win.redraw()
			end
		end

		return win
	end

	function win.redraw()
		if visible then
			-- @todo Localise this
			self:render_to_window( parent, x, y )
		end

		return win
	end

	function win.restoreCursor()
		parent.setCursorBlink( cursor_blink )
		parent.setTextColor( text_colour )

		if cursor_x >= 1 and cursor_y >= 1 and cursor_x <= width and cursor_y <= height then
			parent.setCursorPos( cursor_x + x - 1, cursor_y + y - 1 )
		else
			parent.setCursorPos( 0, 0 )
		end

		return win
	end

	function win.getPosition()
		return x, y
	end

	function win.reposition( new_x, new_y, new_w, new_h )
		new_x = tonumber( new_x ) or error( expected_number_for .. "'new_x'", 2 )
		new_y = tonumber( new_y ) or error( expected_number_for .. "'new_y'", 2 )

		x = new_x
		y = new_y

		if tonumber( new_w ) and tonumber( new_h ) then
			-- @todo Localise this
			self:resize( new_w, new_h, background_colour )

			width = new_w
			height = new_h
		end

		if visible then
			return win.redraw()
		end

		return win
	end

	-- Proper language!
	win.isColour            = win.isColor
	win.setTextColour       = win.setTextColor
	win.getTextColour       = win.getTextColor
	win.setBackgroundColour = win.setBackgroundColor
	win.getBackgroundColour = win.getBackgroundColor

	return win
end

--- Shift the buffer's origin (effectively scrolling the contents).
-- @param x_offset			(Optional) By how much to offset the x coordinate, can be negative, defaults to 0
-- @param y_offset			(Optional) By how much to offset the y coordinate, can be negative, defaults to 0
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:shift_v_dtt( x_offset, y_offset, background_colour, foreground_colour, character )
	local w = self.width
	local h = self.height

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	local start_x, end_x, start_y, end_y
	local x_direction, y_direction

	if y_offset > 0 then
		y_direction = 1
		start_y, end_y = 0, h - 1
	else
		y_direction = -1
		start_y, end_y = h, 0
	end

	if x_offset > 0 then
		x_direction = 1
		start_x, end_x = 0, w - 1
	else
		x_direction = -1
		start_x, end_x = w - 1, 0
	end

	local w_minus_x_offset = w - x_offset
	local h_minus_y_offset = h - y_offset
	local m_x_offset = -x_offset
	local m_y_offset = -y_offset

	for y = start_y, end_y, y_direction do
		local pixel_offset_source = ( y + y_offset ) * w + x_offset
		local pixel_offset_target = y * w

		for x = start_x, end_x, x_direction do
			local pixel

			if x >= m_x_offset and x < w_minus_x_offset and y >= m_y_offset and y < h_minus_y_offset then
				pixel = self[ pixel_offset_source + x ]
			else
				pixel = new_pixel
			end

			self[ pixel_offset_target + x ] = pixel
		end
	end

	return self
end

--- Clear the buffer.
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:clear_v_rtt( background_colour, foreground_colour, character )
	local clear_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                  + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                  + byte[ character or DEFAULT_CHARACTER ]

	-- Go through all pixels and set them to the clear pixel
	for i = 0, self.length do
		self[ i ] = clear_pixel
	end

	return self
end

--- Clear a line of the buffer.
-- @param y					The y coordinate indicating the line to clear, 0-based
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @param start_x			(Optional) The x coordinate to start clearing on, 0-based, defaults to 0
-- @param end_x				(Optional) The x coordinate to stop clearing on, 0-based, defaults to self.width - 1
-- @return self
function buffer_methods:clear_line_v_rtt( y, background_colour, foreground_colour, character, start_x, end_x )
	y = tonumber( y ) or error( expected_number_for .. "'y'", 2 )

	if y < 0 or y >= self.height then
		return self
	end

	local clear_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                  + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                  + byte[ character or DEFAULT_CHARACTER ]

	local w = self.width

	start_x = tonumber( start_x ) or 0
	start_x = start_x > 0 and start_x or 0

	end_x = tonumber( end_x ) or w

	-- Go through all pixels on this line and set them to clear_pixel
	local line_offset = y * w
	for x = start_x, ( end_x < ( w - 1 ) and end_x or w - 1 ) do
		self[ line_offset + x ] = clear_pixel
	end

	return self
end

--- Clear a column of the buffer.
-- @param x					The x coordinate indicating the column to clear, 0-based
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @param start_y			(Optional) The y coordinate to start clearing on, 0-based, defaults to 0
-- @param end_y				(Optional) The y coordinate to stop clearing on, 0-based, defaults to self.height - 1
-- @return self
function buffer_methods:clear_column_v_rtt( x, background_colour, foreground_colour, character, start_y, end_y )
	x = tonumber( x ) or error( expected_number_for .. "'x'", 2 )

	local w = self.width

	if x < 0 or x >= w then
		return self
	end

	local h = self.height

	local clear_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                  + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                  + byte[ character or DEFAULT_CHARACTER ]

	start_y = tonumber( start_y )
	start_y = start_y > 0 and start_y or 0

	end_y = tonumber( end_y ) or h

	-- Go through all pixels in this column and set them to clear_pixel
	for y = start_y, end_y < h and end_y or h do
		self[ y * w + x ] = clear_pixel
	end

	return self
end

--- Write text to the buffer using the specified colours.
--	text, background_colours and foreground_colours have to
--	be of the same length
-- @param x						The x coordinate to write at, 0-based
-- @param y						The y coordinate to write at, 0-based
-- @param text					The text to write
-- @param background_colours	The background colours to use, in paint format (characters)
-- @param foreground_colours	The foreground colours to use, in paint format (characters)
-- @return self
function buffer_methods:blit_v_rtt( x, y, text, background_colours, foreground_colours )
	x = tonumber( x ) or error( expected_number_for .. "'x'", 2 )
	y = tonumber( y ) or error( expected_number_for .. "'y'", 2 )

	local w = self.width

	local text_length = #text

	if text_length ~= #background_colours or text_length ~= #foreground_colours then
		error( "'text', 'background_colours', and 'foreground_colours' all have to be of the same length", 2 )
	end

	background_colours = gsub( background_colours, "%s", "f" )
	foreground_colours = gsub( foreground_colours, "%s", "0" )

	local offset = y * w + x - 1
	text_length  = ( text_length < ( w - x ) and text_length or w - x )

	for i = ( -x > 0 and -x or 0 ) + 1, text_length do
		-- A -1 for i is included in offset
		self[ offset + i ] = colour_lookup[ sub( background_colours, i, i ) ] * PIXEL_VALUE_OFFSET
		                   + colour_lookup[ sub( foreground_colours, i, i ) ]
		                   + byte[ sub( text, i, i ) or DEFAULT_CHARACTER ]
	end

	return self
end

--- Draw a filled, solid colour rectangle using two points.
--	The order of the two points does not matter, i.e. 2:2, 1:1 is the same
--	as 1:1, 2:2
-- @param start_x			The x coordinate to start drawing at, 0-based
-- @param start_y			The y coordinate to start drawing at, 0-based
-- @param end_x				The x coordinate to end drawing at, 0-based
-- @param end_y				The y coordinate to end drawing at, 0-based
-- @param background_colour	(Optional) The background colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the rectangle (including borders) with,
--                 			defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_filled_rectangle_from_points_v_rtt( start_x, start_y, end_x, end_y,
                                                                 background_colour, foreground_colour, character )
	local w = self.width

	-- Starting values must be smaller than ending ones
	if start_x > end_x then
		end_x, start_x = start_x, end_x
	end

	if start_y > end_y then
		end_y, start_y = start_y, end_y
	end

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	-- Go through all pixels of this rectangle and set them to new_pixel
	for y = start_y, end_y do
		local line_offset = y * w

		for x = start_x, end_x do
			self[ line_offset + x ] = new_pixel
		end
	end

	return self
end

--- Draw a filled, solid colour rectangle using one point, width and height.
-- @param x					The x coordinate to start drawing at, 0-based
-- @param y					The y coordinate to start drawing at, 0-based
-- @param width				The width of the rectangle
-- @param height			The height of the rectangle
-- @param background_colour	(Optional) The background colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the rectangle (including borders) with,
--                 			defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_filled_rectangle_v_rtt( x, y, width, height,
                                                     background_colour, foreground_colour, character )
	width = width or height or error( unable_to_set_optional_argument .. "'width': height is nil", 2 )

	local w = self.width
	local end_x = x + width - 1

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	-- Go through all pixels of this rectangle and set them to new_pixel
	for _y = y, y + height - 1 do
		local line_offset = _y * w

		for _x = x, end_x do
			self[ line_offset + _x ] = new_pixel
		end
	end

	return self
end

--- Draw a solid colour rectangle border using two points.
-- @param start_x			The x coordinate to start drawing at, 0-based
-- @param start_y			The y coordinate to start drawing at, 0-based
-- @param end_x				The x coordinate to end drawing at, 0-based
-- @param end_y				The y coordinate to end drawing at, 0-based
-- @param border_width		(Optional) The width of the border (larger values extending to the centre), defaults to 1
-- @param background_colour	(Optional) The background colour to fill the rectangle border with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the rectangle border with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the rectangle border with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_rectangle_from_points_v_rtt( start_x, start_y, end_x, end_y, border_width,
                                                     background_colour, foreground_colour, character )
	border_width = border_width or 1

	-- Starting values must be smaller than ending ones
	-- @todo Add debugging symbols
	if start_x > end_x then
		end_x, start_x = start_x, end_x
	end

	if start_y > end_y then
		end_y, start_y = start_y, end_y
	end

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	local w = self.width

	for i = 0, border_width - 1 do
		local line_offset1 = ( start_y + i ) * w
		local line_offset2 = (   end_y - i ) * w

		for x = start_x + i, end_x - i do
			self[ line_offset1 + x ] = new_pixel
			self[ line_offset2 + x ] = new_pixel
		end

		local column_offset1 = start_x + i
		local column_offset2 =   end_x - i

		for y = start_y + i, end_y - i do
			local line_offset = y * w
			self[ line_offset + column_offset1 ] = new_pixel
			self[ line_offset + column_offset2 ] = new_pixel
		end
	end

	return self
end

--- Draw a solid colour rectangle border using one point, width and height.
-- @param x					(Optional) The x coordinate to start drawing at, 0-based, defaults to 0
-- @param y					(Optional) The y coordinate to start drawing at, 0-based, defaults to 0
-- @param width				(Optional) The width  of the rectangle, defaults to 0
-- @param height			(Optional) The height of the rectangle, defaults to 0
-- @param ...				Any other arguments passed to :draw_rectangle_from_points_v_rtt()
-- @return Tail call of self:draw_rectangle_from_points_v_rtt()
-- @see buffer_methods:draw_rectangle_from_points_v_rtt
function buffer_methods:draw_rectangle_v_rtt( x, y, width, height, ... )
	x = x or 0
	y = y or 0
	width  = width  or 0
	height = height or 0

	-- @todo Localise :draw_rectangle_from_points
	return self:draw_rectangle_from_points_v_rtt( x, y, x + width - 1, y + height - 1, ... )
end

--- Draw a circle outline using the centre point and radius, without corrections for CC rectangular pixels.
-- @param centre_x			The x coordinate of the circle's centre, 0-based
-- @param centre_y			The y coordinate of the circle's centre, 0-based
-- @param radius			The radius of the circle
-- @param background_colour	(Optional) The background colour to fill the circle outline with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the circle outline with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the circle outline with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_circle_raw_v_rtt( centre_x, centre_y, radius,
                                               background_colour, foreground_colour, character )
	local w = self.width

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	local x = radius
	local y = 0
	local err = 0

	-- Generate points for the first octant, going counterclockwise
	-- and mirroring them to other octants
	while x >= y do
		-- Some might say all these locals are an overkill...
		-- To that, I say: screw you! This block of code (including
		-- the self[] sets) is 28 instructions. The previous,
		-- non-micro-optimised approach was 40.

		-- @todo Only redefine c??x variables when x changes

		local cypy = ( centre_y + y ) * w
		local cypx = ( centre_y + x ) * w
		local cymx = ( centre_y - x ) * w
		local cymy = ( centre_y - y ) * w

		local cxpx = centre_x + x
		local cxpy = centre_x + y
		local cxmy = centre_x - y
		local cxmx = centre_x - x

		self[ cypy + cxpx ] = new_pixel
		self[ cypx + cxpy ] = new_pixel
		self[ cymx + cxpy ] = new_pixel
		self[ cymy + cxpx ] = new_pixel

		self[ cypx + cxmy ] = new_pixel
		self[ cypy + cxmx ] = new_pixel
		self[ cymy + cxmx ] = new_pixel
		self[ cymx + cxmy ] = new_pixel

		y = y + 1
		err = err + 1 + 2 * y

		if 2 * ( err - x ) + 1 > 0 then
			x = x - 1
			err = err + 1 - 2 * x
		end
	end

	return self
end

--- Draw a circle outline using the centre point and radius, with corrections for CC rectangular pixels.
-- @param centre_x			The x coordinate of the circle's centre, 0-based
-- @param centre_y			The y coordinate of the circle's centre, 0-based
-- @param radius			The radius of the circle
-- @param background_colour	(Optional) The background colour to fill the circle outline with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the circle outline with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the circle outline with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_circle_v_rtt( centre_x, centre_y, radius, background_colour, foreground_colour, character )
	local w = self.width

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	local x = radius
	local y = 0
	local err = 0

	-- Generate points for the first octant, going counterclockwise
	-- and mirroring them to other octants
	while x >= y do
		-- @todo Only redefine c??x variables when x changes

		local cypy = ( centre_y + round( ( 2 / 3 ) * y ) ) * w
		local cypx = ( centre_y + round( ( 2 / 3 ) * x ) ) * w
		local cymx = ( centre_y - round( ( 2 / 3 ) * x ) ) * w
		local cymy = ( centre_y - round( ( 2 / 3 ) * y ) ) * w

		local cxpx = centre_x + x
		local cxpy = centre_x + y
		local cxmy = centre_x - y
		local cxmx = centre_x - x

		self[ cypy + cxpx ] = new_pixel
		self[ cypx + cxpy ] = new_pixel
		self[ cymx + cxpy ] = new_pixel
		self[ cymy + cxpx ] = new_pixel

		self[ cypx + cxmy ] = new_pixel
		self[ cypy + cxmx ] = new_pixel
		self[ cymy + cxmx ] = new_pixel
		self[ cymx + cxmy ] = new_pixel

		y = y + 1
		err = err + 1 + 2 * y

		if 2 * ( err - x ) + 1 > 0 then
			x = x - 1
			err = err + 1 - 2 * x
		end
	end

	return self
end

--- Draw a filled ellipse.
--	Sorry for the inconsistency here, this code has been borrowed
--	from Xenthera's graphics library
-- @param x					The x coordinate of the ellipse's centre
-- @param y					The y coordinate of the ellipse's centre
-- @param width				The width  of the ellipse
-- @param height			The height of the ellipse
-- @param background_colour	(Optional) The background colour to fill the ellipse with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the ellipse with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the ellipse with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_filled_ellipse_v_rtt( x, y, width, height,
                                                   background_colour, foreground_colour, character )
	local self_width  = self.width
	local self_height = self.height
	local almost_w = self_width - 1

	local hh = height * height
	local ww = width * width
	local hhww = hh * ww
	local x0 = width - 1
	local delta_x = 0

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	if y < self_height and y >= 0 then
		local offset = y * self_width

		local x_p_w = x + width
		local x_m_w = x - width

		for _x = x_m_w > 0 and x_m_w or 0, x_p_w < almost_w and x_p_w or almost_w do
			self[ offset + _x ] = new_pixel
		end
	end

	for _y = 0, height - 1 do
		local x1 = x0 - ( delta_x - 1 )
		local _y_yww = _y * _y * ww

		for _ = x1, 0, -1 do
			if x1 * x1 * hh + _y_yww <= hhww then
				break
			end

			x1 = x1 - 1
		end

		delta_x = x0 - x1
		x0 = x1

		local y_pos1 = y - _y
		local y_pos2 = y + _y

		if y_pos1 >= 0 and y_pos1 < self_height then
			y_pos1 = y_pos1 * self_width

			for _x = ( x - x0 ) > 0 and ( x - x0 ) or 0, ( ( x + x0 ) < almost_w and ( x + x0 ) or almost_w ) do
				self[ y_pos1 + _x ] = new_pixel
			end
		end

		if y_pos2 >= 0 and y_pos2 < self_height then
			y_pos2 = y_pos2 * self_width

			for _x = ( x - x0 ) > 0 and ( x - x0 ) or 0, ( ( x + x0 ) < almost_w and ( x + x0 ) or almost_w ) do
				self[ y_pos2 + _x ] = new_pixel
			end
		end
	end

	return self
end

--- Draw an ellipse outline.
-- @param x					The x coordinate of the ellipse's centre
-- @param y					The y coordinate of the ellipse's centre
-- @param width				The width  of the ellipse
-- @param height			The height of the ellipse
-- @param background_colour	(Optional) The background colour to fill the ellipse with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the ellipse with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the ellipse with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_ellipse_v_rtt( centre_x, centre_y, width, height,
                                            background_colour, foreground_colour, character )
	local self_width  = self.width

	local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
	                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
	                + byte[ character or DEFAULT_CHARACTER ]

	local a2 = width  * width
	local b2 = height * height

	local four_a2 = 4 * a2
	local four_b2 = 4 * b2

	local x, y, sigma = 0, height, 2 * b2 + a2 * ( 1 - 2 * height )

	local offset1 = self_width * ( centre_y + y ) + centre_x
	local offset2 = self_width * ( centre_y - y ) + centre_x

	-- Bounds checking, normally 0 <= line_offset <= self.length,
	-- here -centre_x <= offset <= self.length - centre_x
	local len = self.length
	local min = -centre_x
	local max = self_width - centre_x - 1

	local do_draw1 = 0 <= offset1 and offset1 <= len
	local do_draw2 = 0 <= offset2 and offset2 <= len

	local a2y = a2 * y

	while b2 * x <= a2y do
		-- Whether to draw +x indices
		local do_draw_px = min <=  x and  x <= max
		-- Whether to draw -x indices
		local do_draw_mx = min <= -x and -x <= max

		if do_draw1 then
			if do_draw_px then
				self[ offset1 + x ] = new_pixel
			end

			if do_draw_mx then
				self[ offset1 - x ] = new_pixel
			end
		end

		if do_draw2 then
			if do_draw_px then
				self[ offset2 + x ] = new_pixel
			end

			if do_draw_mx then
				self[ offset2 - x ] = new_pixel
			end
		end

		if sigma >= 0 then
			sigma = sigma + four_a2 * ( 1 - y )

			y = y - 1
			-- Update y-dependant values
			offset1 = self_width * ( centre_y + y ) + centre_x
			offset2 = self_width * ( centre_y - y ) + centre_x

			do_draw1 = 0 <= offset1 and offset1 <= len
			do_draw2 = 0 <= offset2 and offset2 <= len

			a2y = a2 * y
		end

		sigma = sigma + b2 * ( ( 4 * x ) + 6 )

		x = x + 1
	end

	x, y, sigma = width, 0, 2 * a2 + b2 * ( 1 - 2 * width )

	local b2x = b2 * x

	-- Whether to draw +x indices
	local do_draw_px = min <=  x and  x <= max
	-- Whether to draw -x indices
	local do_draw_mx = min <= -x and -x <= max

	while a2 * y <= b2x do
		offset1 = self_width * ( centre_y + y ) + centre_x
		offset2 = self_width * ( centre_y - y ) + centre_x

		do_draw1 = 0 <= offset1 and offset1 <= len
		do_draw2 = 0 <= offset2 and offset2 <= len


		if do_draw1 then
			if do_draw_px then
				self[ offset1 + x ] = new_pixel
			end

			if do_draw_mx then
				self[ offset1 - x ] = new_pixel
			end
		end

		if do_draw2 then
			if do_draw_px then
				self[ offset2 + x ] = new_pixel
			end

			if do_draw_mx then
				self[ offset2 - x ] = new_pixel
			end
		end

		if sigma >= 0 then
			sigma = sigma + four_b2 * ( 1 - x )

			x = x - 1
			-- Update x-dependant values
			b2x = b2 * x

			do_draw_px = min <=  x and  x <= max
			do_draw_mx = min <= -x and -x <= max
		end

		sigma = sigma + a2 * ( ( 4 * y ) + 6 )

		y = y + 1
	end

	return self
end

--- Write text to the buffer.
-- @param x					The x coordinate to start writing at, 0-based
-- @param y					The y coordinate to start writing at, 0-based
-- @param text				The text to write
-- @param background_colour	(Optional) The background colour to use, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to use, defaults to DEFAULT_FOREGROUND
-- @return self
function buffer_methods:write_v_rtt( x, y, text, background_colour, foreground_colour )
	background_colour = background_colour or DEFAULT_BACKGROUND
	foreground_colour = foreground_colour or DEFAULT_FOREGROUND

	local width = self.width

	if x < width and y >= 0 and y < self.height then
		local base = background_colour * PIXEL_VALUE_OFFSET + foreground_colour
		local len = #text

		if len == 1 then
			-- Avoid calling sub() altogether if the string is only a single character
			if x >= 0 then
				self[ y * width + x ] = base + byte[ text ]
			end

		else
			local line_offset = y * width + x - 1
			local last_pixel, last_char

			-- Go through the string, writing the new pixels
			for i = ( -x > 0 and -x or 0 ) + 1, ( len < ( width - x ) and len or width - x ) do
				local ch = byte[ sub( text, i, i ) ]

				if ch ~= last_char then
					last_pixel = base + ch

					last_char = ch
				end

				-- A -1 for i is included in line_offset
				local index = line_offset + i
				self[ index ] = last_pixel
			end
		end
	end

	return self
end

--- Iterate over the buffer.
-- @param start_x	(Optional) The x coordinate to start iterating from, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate to start iterating from, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate to end iterating at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate to end iterating at, 0-based, defaults to self.height - 1
-- @return fn An iterator function which, when called, returns the next pixel, its coordinates and its index
function buffer_methods:iter_v( start_x, start_y, end_x, end_y )
	local w = self.width

	local x = ( start_x or 0 ) - 1
	local y = start_y or 0

	end_x   = end_x   or self.width - 1
	end_y   = end_y   or self.height - 1

	local line_offset = y * w

	return function()
		x = x + 1

		if x > end_x then
			y = y + 1

			if y > end_y then
				return nil
			end

			line_offset = y * w
			x = 0
		end

		local index = line_offset + x

		return self[ index ], x, y, index
	end
end

--- Shrink the buffer contents using teletext characters from CC 1.76+
--	The shrinking algorithm itself has been adapted from CrazedProgrammer's
--	[Surface2](https://github.com/CrazedProgrammer/Surface2), used with permission.
--	Thanks Crazed!
-- @param target	(Optional) The target buffer, defaults to self.parent
-- @param x			(Optional) The x coordinate in target to shrink self to, 0-based, defaults to self.x1
-- @param y			(Optional) The y coordinate in target to shrink self to, 0-based, defaults to self.y1
-- @param x_offset	(Optional) The number of mini pixels to offset x by, 0 - 1, defaults to 0
-- @param y_offset	(Optional) The number of mini pixels to offset y by, 0 - 2, defaults to 0
-- @param start_x	(Optional) The x coordinate in self to start shrinking from, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate in self to start shrinking from, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate in self to stop shrinking at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate in self to stop shrinking at, 0-based, defaults to self.height - 1
-- @return self
function buffer_methods:shrink_v( target, x, y, start_x, start_y, end_x, end_y )
	target = target or self.parent or error( unable_to_set_optional_argument .. "'target': self.parent is nil", 2 )
	x      = x      or self.x1     or error( unable_to_set_optional_argument .. "'x': self.x1 is nil", 2 )
	y      = y      or self.y1     or error( unable_to_set_optional_argument .. "'y': self.y1 is nil", 2 )

	local target_width = target.width
	local self_width   =   self.width

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or ( self_width  - 1 ) / 2
	end_y = end_y or ( self.height - 1 ) / 3

	start_x = start_x - start_x % 1
	start_y = start_y - start_y % 1
	end_x   = end_x   - end_x   % 1
	end_y   = end_y   - end_y   % 1

	local foreground_colour, c1, c2, c3, c4, c5, c6, character = DEFAULT_FOREGROUND

	for _y = start_y, end_y do
		local offset1 = self_width * 3 * _y
		local offset2 = self_width + offset1
		local offset3 = self_width + offset2

		local target_offset = ( _y + y ) * target_width + x

		for _x = start_x, end_x do
			local two_x = 2 * _x

			character = 128
			c1, c2, c3, c4, c5, c6 = self[ offset1 + two_x     ] / PIXEL_VALUE_OFFSET,
			                         self[ offset1 + two_x + 1 ] / PIXEL_VALUE_OFFSET,
			                         self[ offset2 + two_x     ] / PIXEL_VALUE_OFFSET,
			                         self[ offset2 + two_x + 1 ] / PIXEL_VALUE_OFFSET,
			                         self[ offset3 + two_x     ] / PIXEL_VALUE_OFFSET,
			                         self[ offset3 + two_x + 1 ] / PIXEL_VALUE_OFFSET

			if c1 ~= c6 then
				foreground_colour = c1
				character = character + 1
			end
			if c2 ~= c6 then
				foreground_colour = c2
				character = character + 2
			end
			if c3 ~= c6 then
				foreground_colour = c3
				character = character + 4
			end
			if c4 ~= c6 then
				foreground_colour = c4
				character = character + 8
			end
			if c5 ~= c6 then
				foreground_colour = c5
				character = character + 16
			end

			target[ target_offset + _x ] = c6 * PIXEL_VALUE_OFFSET + foreground_colour + character
		end
	end

	return self
end

--- Render the buffer to a terminal.
-- @param target	(Optional) The target IO stream, defaults to io.output
-- @param x			(Optional) The x coordinate to render the buffer at, 1-based, defaults to self.x1 + 1
-- @param y			(Optional) The y coordinate to render the buffer at, 1-based, defaults to self.y1 + 1
-- @param parent	(Optional) The buffer to use for transparency resolution, defaults to self.parent
-- @param start_x	(Optional) The x coordinate to start rendering from, defaults to 0
-- @param start_y	(Optional) The y coordinate to start rendering from, defaults to 0
-- @param end_x		(Optional) The x coordinate to end rendering at, defaults to self.width
-- @param end_y		(Optional) The y coordinate to end rendering at, defaults to self.height
-- @return self
function buffer_methods:render_to_term_v_rtt( target, x, y, parent, start_x, start_y, end_x, end_y )
	target = target or io.output()

	x = x or self.x1 + 1
	y = y or self.y1 + 1

	local width = self.width
	local almost_w = width - 1
	local almost_h = self.height - 1

	if  start_x then
		start_x = start_x < 0 and 0 or start_x
	else
		start_x = 0
	end

	if  end_x then
		end_x = end_x > almost_w and almost_w or end_x
	else
		end_x = almost_w
	end

	if end_x < start_x then
		start_x, end_x = end_x, start_x
	end

	if  start_y then
		start_y = start_y < 0 and 0 or start_y
	else
		start_y = 0
	end

	if  end_y then
		end_y = end_y > almost_h and almost_h or end_y
	else
		end_y = almost_h
	end

	if end_y < start_y then
		start_y, end_y = end_y, start_y
	end

	local write = target.write

	local false_parent = { parent = parent }

	local last_bg, last_fg

	-- Start the stream with the cursor position
	local characters = {
		27, 91, string_byte( tostring( y ) )
	}

	local n_characters = #characters + 1
	characters[ n_characters ] = 59	-- ';'

	local column = { string_byte( tostring( x ) ) }
	for i = 1, #column do
		n_characters = n_characters + 1
		characters[ n_characters ] = column[ i ]
	end

	n_characters = n_characters + 1
	characters[ n_characters ] = 72	-- 'H'

	for _y = start_y, end_y do
		local line_offset = _y * width

		for _x = start_x, end_x do
			local pixel = self[ line_offset + _x ]

			local bg = pixel /     PIXEL_VALUE_OFFSET
			local fg = pixel %     PIXEL_VALUE_OFFSET
			local ch = pixel % 1 * PIXEL_VALUE_OFFSET

			-- Set the pixel in target, resolving transparency along the way
			if bg >= TRANSPARENT_VALUES or fg >= TRANSPARENT_VALUES or ch == TRANSPARENT_CHARACTER then
				local local_parent = false_parent

				local background_colour = bg
				local foreground_colour = fg
				local character = ch

				local tracked_offset_x = x
				local tracked_offset_y = y

				-- Not only we have to get to the non-transparent colour
				-- when the background_colour is transparent, we also have
				-- to resolve the colour if the *foreground* uses it

				if background_colour >= TRANSPARENT_VALUES or fg >= TRANSPARENT_BACKGROUND then
					local tracked_colour = TRANSPARENT_BACKGROUND

					-- Down into the rabbit hole we go. One parent level further with every iteration.
					-- We have to let the loop run at least once, for the TRANSPARENT_BACKGROUND to resolve
					repeat
						-- This wouldn't work the first time (when the loop starts), that's
						-- why we defined local_parent to be the false_parent
						local_parent = local_parent.parent

						if not local_parent then
							-- We've reached the very bottom of the family tree, without luck
							background_colour = DEFAULT_BACKGROUND
							break
						end

						-- All buffers have a position, we have to keep track of that
						tracked_offset_x = tracked_offset_x + local_parent.x1
						tracked_offset_y = tracked_offset_y + local_parent.y1

						local actual_y = _y + tracked_offset_y
						local actual_x = _x + tracked_offset_x
						local p_width  = local_parent.width
						local p_height = local_parent.height

						-- Check that we are within bounds
						-- We could subtract a 1 in the local definition above, but if actual_x < 0 then the operation
						-- doesn't even happen :P
						if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
							-- We can't get a pixel out of bounds of this parent, so we'll just roll with
							-- the default colour.
							-- @todo Would it be better if we'd look for any parent that *is* within bounds instead?
							--       Probably not, the result isn't visible anyway since that part of self is outside
							--       of the parent
							background_colour = DEFAULT_BACKGROUND
							break
						end

						local parent_pixel = local_parent[ actual_y * p_width + actual_x ]

						if tracked_colour == TRANSPARENT_BACKGROUND then
							background_colour = parent_pixel / PIXEL_VALUE_OFFSET
						else
							background_colour = parent_pixel % PIXEL_VALUE_OFFSET
						end

						tracked_colour = background_colour - background_colour % 1

					until background_colour < TRANSPARENT_VALUES
				end

				-- The same goes for the foreground colour...
				if foreground_colour >= TRANSPARENT_VALUES or ( bg < TRANSPARENT_BACKGROUND and bg >= TRANSPARENT_FOREGROUND ) then

					local_parent = false_parent
					tracked_offset_x = x
					tracked_offset_y = y

					local tracked_colour = TRANSPARENT_FOREGROUND

					repeat
						local_parent = local_parent.parent

						if not local_parent then
							foreground_colour = DEFAULT_FOREGROUND
							break
						end

						tracked_offset_x = tracked_offset_x + local_parent.x1
						tracked_offset_y = tracked_offset_y + local_parent.y1

						local actual_y = _y + tracked_offset_y
						local actual_x = _x + tracked_offset_x
						local p_width  = local_parent.width
						local p_height = local_parent.height

						if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
							foreground_colour = DEFAULT_FOREGROUND
							break
						end

						local parent_pixel = local_parent[ actual_y * p_width + actual_x ]

						if tracked_colour == TRANSPARENT_FOREGROUND then
							foreground_colour = parent_pixel % PIXEL_VALUE_OFFSET
						else
							foreground_colour = parent_pixel / PIXEL_VALUE_OFFSET
						end

						tracked_colour = foreground_colour - foreground_colour % 1

					until foreground_colour < TRANSPARENT_VALUES
				end

				-- ...And finally for the character.
				-- Note that a colour cannot be transparent to a character, so
				-- there's no extra branch here
				local_parent = false_parent
				tracked_offset_x = x
				tracked_offset_y = y

				while character == TRANSPARENT_CHARACTER do
					local_parent = local_parent.parent

					if not local_parent then
						character = DEFAULT_CHARACTER
						break
					end

					tracked_offset_x = tracked_offset_x + local_parent.x1
					tracked_offset_y = tracked_offset_y + local_parent.y1

					local actual_y = _y + tracked_offset_y
					local actual_x = _x + tracked_offset_x
					local p_width  = local_parent.width
					local p_height = local_parent.height

					if actual_x < 0 or actual_x > p_width - 1 or actual_y < 0 or actual_y > p_height - 1 then
						character = DEFAULT_CHARACTER
						break
					end

					character = local_parent[ actual_y * p_width + actual_x ] % 1 * PIXEL_VALUE_OFFSET
				end

				-- Assign the proper data to the rendered pixel
				-- @todo Rewrite as if-else blocks and simplify
				bg = bg - bg % 1
				fg = fg - fg % 1
				background_colour = background_colour - background_colour % 1
				foreground_colour = foreground_colour - foreground_colour % 1

				bg = bg <  TRANSPARENT_VALUES and bg or ( bg == TRANSPARENT_BACKGROUND and background_colour or foreground_colour )
				fg = fg <  TRANSPARENT_VALUES and fg or ( fg == TRANSPARENT_FOREGROUND and foreground_colour or background_colour )

				ch = ch ~= TRANSPARENT_CHARACTER and ch or character

			else
				bg = bg - bg % 1
				fg = fg - fg % 1
			end

			if bg ~= last_bg then
				characters[ n_characters + 1  ] = 27					-- ESC character
				characters[ n_characters + 2  ] = 91					-- '['
				characters[ n_characters + 3  ] = 52					-- '4'
				characters[ n_characters + 4  ] = 56					-- '8'
				characters[ n_characters + 5  ] = 59					-- ';'
				characters[ n_characters + 6  ] = 53					-- '5'
				characters[ n_characters + 7  ] = 59					-- ';'
				characters[ n_characters + 8  ] = ANSI_palette1[ bg ]	-- The new background colour (3 digits)
				characters[ n_characters + 9  ] = ANSI_palette2[ bg ]
				characters[ n_characters + 10 ] = ANSI_palette3[ bg ]
				characters[ n_characters + 11 ] = 109					-- 'm'

				n_characters = n_characters + 11

				last_bg = bg
			end

			if fg ~= last_fg then
				characters[ n_characters + 1  ] = 27
				characters[ n_characters + 2  ] = 91
				characters[ n_characters + 3  ] = 51
				characters[ n_characters + 4  ] = 56
				characters[ n_characters + 5  ] = 59
				characters[ n_characters + 6  ] = 53
				characters[ n_characters + 7  ] = 59
				characters[ n_characters + 8  ] = ANSI_palette1[ fg ]
				characters[ n_characters + 9  ] = ANSI_palette2[ fg ]
				characters[ n_characters + 10 ] = ANSI_palette3[ fg ]
				characters[ n_characters + 11 ] = 109

				n_characters = n_characters + 11

				last_fg = fg
			end

			n_characters = n_characters + 1
			characters[ n_characters ] = ch
		end

		n_characters = n_characters + 1
		characters[ n_characters ] = 10	-- newline
	end

	local mod = n_characters % MAX_UNPACK_SIZE

	for i = 1, n_characters - mod, MAX_UNPACK_SIZE do
		write( target, string_char( unpack( characters, i, i + MAX_UNPACK_SIZE ) ) )
	end

	write( target, string_char( unpack( characters, n_characters - mod, n_characters ) ) .. "\27[0m" )
end

-- Aliases
buffer_methods.draw_rect = buffer_methods.draw_rectangle
buffer_methods.draw_rect_from_points = buffer_methods.draw_rectangle_from_points
buffer_methods.draw_filled_rect = buffer_methods.draw_filled_rectangle
buffer_methods.draw_filled_rect_from_points = buffer_methods.draw_filled_rectangle_from_points

buffer_methods.iterate = buffer_methods.iter

--- Load a buffer from a byte stream.
-- @param stream			description
-- @param x1				description
-- @param y1				description
-- @param parent			description
-- @param existing_table	description
-- @return The new buffer object
function buffer.unserialise( stream, x1, y1, parent, existing_table )
	local n = setmetatable( existing_table or {}, buffer_metatable )

	x1 = x1 or 0
	y1 = y1 or 0

	local width, height, bg_controls, fg_controls, char_controls

	-- Skip the first two bytes as they are simply "DX"
	width  = 2 ^ 8 * stream[ 3 ] + stream[ 4 ]
	height = 2 ^ 8 * stream[ 5 ] + stream[ 6 ]

	bg_controls   = 2 ^ 8 * stream[ 7  ] + stream[ 8  ]
	fg_controls   = 2 ^ 8 * stream[ 9  ] + stream[ 10 ]
	char_controls = 2 ^ 8 * stream[ 11 ] + stream[ 12 ]

	print( "\
Parsed header:\
width = " .. width .. "\
height = " .. height .. "\
bg_controls = " .. bg_controls .. "\
fg_controls = " .. fg_controls .. "\
char_controls = " .. char_controls .. "\
" )

	local i = 12

	-- Offset into n
	local offset = -1

	for _ = 1, bg_controls do
		i = i + 1
		local _byte = stream[ i ]

		print( "Read control byte " .. _byte )

		if _byte <= 128 then
			-- Unique data
			print( "  -> unique" )
			for _ = 0, _byte do
				i = i + 1
				offset = offset + 1
				n[ offset ] = { stream[ i ], DEFAULT_FOREGROUND, DEFAULT_CHARACTER }
			end
		else
			-- Repetition
			print( "  -> repetitive" )
			i = i + 1
			local colour = stream[ i ]

			for _ = 128, _byte do
				offset = offset + 1
				n[ offset ] = { colour, DEFAULT_FOREGROUND, DEFAULT_CHARACTER }
			end
		end
	end

	-- Copy the buffer methods to this instance
	for k, fn in pairs( buffer_methods ) do
		n[ k ] = fn
	end

	n.x1 = x1
	n.y1 = y1
	n.x2 = x1 + width  - 1
	n.y2 = y1 + height - 1

	n.width  = width
	n.height = height
	n.length = width * height - 1
	n.parent = parent

	-- Metadata
	n.__type = "desktox.buffer"

	return n
end

--- Create a new buffer using one point, width and height.
-- @param x			(Optional) The x coordinate of the buffer in parent, 0-based, defaults to 0
-- @param y			(Optional) The y coordinate of the buffer in parent, 0-based, defaults to 0
-- @param width		(Optional) The width of the buffer, defaults to 0
-- @param height	(Optional) The height of the buffer, defaults to 0
-- @param ...		(Optional) Any other arguments passed to buffer.new_from_points
-- @return Tail call of buffer.new_from_points(), resulting in the new buffer
-- @see buffer.new_from_points
function buffer.new_v_rtt( x, y, width, height, ... )
	-- @todo Localise .new_from_points
	return buffer.new_from_points( x, y, x and ( x + width - 1 ) or 0, y and ( y + height - 1 ) or 0, ... )
end

--- Create a new buffer using two points.
-- @param x1				(Optional) The x coordinate of the first point of the buffer in parent, 0-based, defaults to 0
-- @param y1				(Optional) The y coordinate of the first point of the buffer in parent, 0-based, defaults to 0
-- @param x2				(Optional) The x coordinate of the second point of the buffer in parent, 0-based, defaults to 0
-- @param y2				(Optional) The y coordinate of the second point of the buffer in parent, 0-based, defaults to 0
-- @param parent			This buffer's render target
-- @param background_colour	(Optional) The background colour to prefill the buffer with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to prefill the buffer with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to prefill the buffer with, defaults to DEFAULT_CHARACTER
-- @param no_prefill		(Optional) Disable prefilling of the buffer, defaults to false
-- @param existing_table	(Optional) Use this table for the object instead of creating a new one
-- @return buffer			The new buffer
function buffer.new_from_points_v_rtt( x1, y1, x2, y2, parent,
                                       background_colour, foreground_colour, character, no_prefill, existing_table )
	local n = setmetatable( existing_table or {}, buffer_metatable )

	x1 = x1 or 0
	y1 = y1 or 0
	x2 = x2 or 0
	y2 = y2 or 0

	-- Ending values must be larger than starting ones
	if x1 > x2 then
		x2, x1 = x1, x2
	end

	if y1 > y2 then
		y2, y1 = y1, y2
	end

	local width  = x2 - x1 + 1
	local height = y2 - y1 + 1

	local length = width * height - 1

	if not no_prefill then
		local new_pixel = ( tonumber( background_colour ) or DEFAULT_BACKGROUND ) * PIXEL_VALUE_OFFSET
		                + ( tonumber( foreground_colour ) or DEFAULT_FOREGROUND )
		                + byte[ character or DEFAULT_CHARACTER ]

		-- Prefill the buffer with new_pixel
		for i = 0, length do
			n[ i ] = new_pixel
		end
	end

	-- Copy the buffer methods to this instance
	for k, fn in pairs( buffer_methods ) do
		n[ k ] = fn
	end

	n.x1 = x1
	n.y1 = y1
	n.x2 = x2
	n.y2 = y2

	n.width  = width
	n.height = height
	n.length = length
	n.parent = parent

	-- Metadata
	n.__type = "desktox.buffer"

	return n
end

--- Convert the API functions and methods to the desired case.
-- @param fn			The function to use for the coversion
-- @param find_callback	(Optional) The callback to desktox:find_variant(). When set, the default variant
--                     	is determined and stored in the converted table (such as
--                     	buffer_methods:render->buffer_methods:render_v_rtt), not used by default
-- @return nil
function buffer.cook_API_v( fn, find_callback )
	local tables = { buffer_methods, buffer }
	local cooked = { cooked_methods, cooked_lib_methods }

	for i = 1, #tables do
		local tbl = tables[ i ]

		local n_keys = 0
		local keys = {}

		-- Get all the keys
		for key, _ in pairs( tbl ) do
			n_keys = n_keys + 1
			keys[ n_keys ] = key
		end

		-- Convert the keys
		for ii = 1, n_keys do
			local key = keys[ ii ]
			local name, variant = match( key, "^(.*)(_v%d*_?%w*[%w_]-)$" )

			if name then
				local new = fn( name )

				if find_callback and not tbl[ new ] then
					tbl[ new ] = find_callback( cooked[ i ], name )._fn
				end

				tbl[ new .. variant ] = tbl[ key ]
			end
		end
	end
end

-- Export the complete ((cooked )?method |meta)table
buffer.cooked_lib_methods = cooked_lib_methods
buffer.cooked_methods = cooked_methods
buffer.methods  = buffer_methods
buffer.lib_meta = lib_metatable
buffer.meta     = buffer_metatable

--- Export utility functions and other locally-defined data.
-- @return nil
function buffer.set_aliases()
	-- Exports
	buffer.clone  = buffer_methods.clone
	buffer.repair = buffer_methods.repair

	-- Export utility functions
	buffer.round = round

	-- Export the default values
	buffer.DEFAULT_BACKGROUND = DEFAULT_BACKGROUND
	buffer.DEFAULT_FOREGROUND = DEFAULT_FOREGROUND
	buffer.DEFAULT_CHARACTER  = DEFAULT_CHARACTER

	-- Export transparency values
	buffer.TRANSPARENT_BACKGROUND = TRANSPARENT_BACKGROUND
	buffer.TRANSPARENT_FOREGROUND = TRANSPARENT_FOREGROUND
	buffer.TRANSPARENT_CHARACTER  = TRANSPARENT_CHARACTER
	buffer.TRANSPARENT_PIXEL      = TRANSPARENT_PIXEL

	-- Export all other colours
	buffer.colours = colours
	buffer.colors  = colours
end

--- Syntactic sugar for both :render() and :render_to_window().
--	Will attempt to decide which of the 2 methods to use, based
--	on the given arguments. Defaults to :render().
--	Should be *avoided* for best performance.
-- @param target	The buffer or window to render to
-- @param ...		Any arguments passed to :render() or :render_to_window()
-- @return Tail call of :render() or :render_to_window()
function buffer_metatable:__call( target, ... )
	-- @todo Localise the methods
	if type( target ) == "table" then
		if target.setCursorPos then
			return self:render_to_window( target, ... )
		end
	end

	return self:render( target or false, ... )
end

-- Export transparency values
buffer.TRANSPARENT_BACKGROUND = TRANSPARENT_BACKGROUND
buffer.TRANSPARENT_FOREGROUND = TRANSPARENT_FOREGROUND
buffer.TRANSPARENT_CHARACTER  = TRANSPARENT_CHARACTER
buffer.TRANSPARENT_PIXEL      = TRANSPARENT_PIXEL

-- Export all other colours
buffer.colours = colours
buffer.colors  = colours

return buffer
