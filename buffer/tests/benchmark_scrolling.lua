
local desktox = dofile "init.lua" "snake_case"
local buffer = desktox:load "buffer/init.lua"

local buf = buffer.new( 0, 0, 51, 19 )
local shift = buf.shift

local clock = os.clock
local limit = 5000

local start = clock()
for _ = 1, limit do
	shift( buf, 20, 15 )
end

local total = clock() - start

print( "case", "realistic" )
print( "time", total )
print( "runs", limit )
print( "desc", "`buffer:shift( 20, 15 )` calls per second for a 51x19 buffer." )
