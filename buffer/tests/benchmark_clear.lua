
local desktox = dofile "init.lua" "snake_case"
local buffer = desktox:load "buffer/init.lua"

local buf = buffer.new( 0, 0, 51, 19 )
local clear = buf.clear

local clock = os.clock
local limit = 10000

local start = clock()
for _ = 1, limit do
	clear( buf )
end

local total = clock() - start

print( "case", '"realistic"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"`buffer:clear()` calls per second for a 51x19 buffer."' )

buf = buffer.new( 0, 0, 100, 100 )

limit = 1000
start = clock()

for _ = 1, limit do
	clear( buf )
end

total = clock() - start

print( "case", '"scale test"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"`buffer:clear()` calls per second for a 100x100 buffer."' )

