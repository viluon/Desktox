
local desktox = dofile "init.lua" "snake_case"
local buffer = desktox:load "buffer/init.lua"

local n = buffer.new_from_points

local clock = os.clock
local limit = 10000

local start = clock()
for _ = 1, limit do
	local _ = n( 0, 0, 51, 19 )
end

local total = clock() - start

print( "case", '"realistic"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"Constructor calls per second for a 51x19 buffer."' )

limit = 1000
start = clock()

for _ = 1, limit do
	local _ = n( 0, 0, 100, 100 )
end

total = clock() - start

print( "case", '"scale test"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"Constructor calls per second for a 100x100 buffer."' )
