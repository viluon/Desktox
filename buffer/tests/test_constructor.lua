
local desktox = dofile "init.lua" "snake_case"
local buffer = desktox:load "buffer/init.lua"

io.write( "Testing constructor... " )
local main = buffer.new()

assert( main.x1 == 0, "Wrong default" )
assert( main.y1 == 0, "Wrong default" )
assert( main.width == 1, "Wrong default" )
assert( main.height == 1, "Wrong default" )

assert( buffer.methods, "Missing methods table" )

for k, v in pairs( buffer.methods ) do
	assert( main[ k ] == v, "Wrong method: " .. tostring( k ) )
end

print( "Passed" )
