
local buffer_methods = {}

-- Constants
local PIXEL_VALUE_OFFSET = 256

local TRANSPARENT_VALUES = 254

local TRANSPARENT_BACKGROUND = 254
local TRANSPARENT_FOREGROUND = 255
local TRANSPARENT_CHARACTER  = 0

local TRANSPARENT_PIXEL = TRANSPARENT_BACKGROUND * PIXEL_VALUE_OFFSET
                        + TRANSPARENT_FOREGROUND
                        + TRANSPARENT_CHARACTER

local DEFAULT_BACKGROUND = 48
local DEFAULT_FOREGROUND = 102
local DEFAULT_CHARACTER  = ' '

-- Error messages
local unable_to_set_optional_argument = "Unable to set optional argument "
local expected_number_for = "Expected number for "

-- Utilities
local next = next
local error = error
local tonumber = tonumber
local string_byte = string.byte
local string_char = string.char
local gsub = string.gsub
local  sub = string.sub

local byte = setmetatable( {}, {
	__index = function( t, k )
		local b = string_byte( k ) / PIXEL_VALUE_OFFSET
		t[ k ] = b
		return b
	end;
} )

local colours   = {
	white		=  48;
	orange		=  49;
	magenta		=  50;
	lightBlue	=  51;
	yellow		=  52;
	lime		=  53;
	pink		=  54;
	grey		=  55;
	lightGrey	=  56;
	cyan		=  57;
	purple		=  97;
	blue		=  98;
	brown		=  99;
	green		= 100;
	red			= 101;
	black		= 102;

	TRANSPARENT_BACKGROUND = TRANSPARENT_BACKGROUND;
	TRANSPARENT_FOREGROUND = TRANSPARENT_FOREGROUND;
}

-- An array of known colours
local colour_array = {
	colours.white,
	colours.orange,
	colours.magenta,
	colours.lightBlue,
	colours.yellow,
	colours.lime,
	colours.pink,
	colours.grey,
	colours.lightGrey,
	colours.cyan,
	colours.purple,
	colours.blue,
	colours.brown,
	colours.green,
	colours.red,
	colours.black,
	TRANSPARENT_BACKGROUND,
	TRANSPARENT_FOREGROUND,
}

-- Generate a bidirectional lookup table
local colour_lookup = {}
for i = 1, #colour_array do
	local colour = colour_array[ i ]
	local hex = sub( "0123456789abcdefgh", i, i )

	colour_lookup[ colour ] = hex
	colour_lookup[ hex ] = colour
end

--- Round a number to a set amount of decimal places.
-- @param n			The number to round
-- @param places	(Optional) The amount of decimal places to keep, defaults to 0
-- @return The rounded number
local function round( n, places )
	local mult = 10 ^ ( places or 0 )
	local m = n * mult + 0.5

	return ( m - m % 1 ) / mult
end

--- Clear the buffer.
-- @variant Draw-time transparency.
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:clear_v_dtt( background_colour, foreground_colour, character )
	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local clear_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		clear_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		clear_pixel = clear_pixel + foreground_colour
	end

	if ch_solid then
		clear_pixel = clear_pixel + char_code
	end

	-- Go through all pixels and set them to the clear pixel
	for i = 0, self.length do
		local pixel = clear_pixel
		local underneath = self[ i ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ i ] = pixel
	end

	return self
end

--- Clear a line of the buffer.
-- @variant Draw-time transparency.
-- @param y					The y coordinate indicating the line to clear, 0-based
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @param start_x			(Optional) The x coordinate to start clearing on, 0-based, defaults to 0
-- @param end_x				(Optional) The x coordinate to stop clearing on, 0-based, defaults to self.width - 1
-- @return self
function buffer_methods:clear_line_v_dtt( y, background_colour, foreground_colour, character, start_x, end_x )
	y = tonumber( y ) or error( expected_number_for .. "'y'", 2 )

	if y < 0 or y >= self.height then
		return self
	end

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local clear_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		clear_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		clear_pixel = clear_pixel + foreground_colour
	end

	if ch_solid then
		clear_pixel = clear_pixel + char_code
	end

	local w = self.width

	start_x = tonumber( start_x ) or 0
	start_x = start_x > 0 and start_x or 0

	end_x = tonumber( end_x ) or w

	-- Go through all pixels on this line and set them to clear_pixel
	local line_offset = y * w
	for i = line_offset + start_x, line_offset + ( end_x < ( w - 1 ) and end_x or w - 1 ) do
		local pixel = clear_pixel
		local underneath = self[ i ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ i ] = pixel
	end

	return self
end

--- Clear a column of the buffer.
-- @variant Draw-time transparency.
-- @param x					The x coordinate indicating the column to clear, 0-based
-- @param background_colour	(Optional) The background colour to clear with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to clear with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to clear with, defaults to DEFAULT_CHARACTER
-- @param start_y			(Optional) The y coordinate to start clearing on, 0-based, defaults to 0
-- @param end_y				(Optional) The y coordinate to stop clearing on, 0-based, defaults to self.height - 1
-- @return self
function buffer_methods:clear_column_v_dtt( x, background_colour, foreground_colour, character, start_y, end_y )
	x = tonumber( x ) or error( expected_number_for .. "'x'", 2 )

	local w = self.width

	if x < 0 or x >= w then
		return self
	end

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local clear_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		clear_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		clear_pixel = clear_pixel + foreground_colour
	end

	if ch_solid then
		clear_pixel = clear_pixel + char_code
	end

	local h = self.height

	start_y = tonumber( start_y )
	start_y = start_y and ( start_y > 0 and start_y ) or 0

	end_y = tonumber( end_y ) or h

	-- Go through all pixels in this column and set them to clear_pixel
	for y = start_y, ( end_y < h and end_y or h ) - 1 do
		local i = y * w + x

		local pixel = clear_pixel
		local underneath = self[ i ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ i ] = pixel
	end

	return self
end


--- Write text to the buffer.
-- @variant Draw-time transparency.
-- @param x					The x coordinate to start writing at, 0-based
-- @param y					The y coordinate to start writing at, 0-based
-- @param text				The text to write
-- @param background_colour	(Optional) The background colour to use, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to use, defaults to DEFAULT_FOREGROUND
-- @return self
function buffer_methods:write_v_dtt( x, y, text, background_colour, foreground_colour )
	background_colour = background_colour or DEFAULT_BACKGROUND
	foreground_colour = foreground_colour or DEFAULT_FOREGROUND

	local width = self.width

	if x < width and y >= 0 and y < self.height then
		local base = 0

		local bg_solid = background_colour < TRANSPARENT_VALUES
		local fg_solid = foreground_colour < TRANSPARENT_VALUES

		if bg_solid then
			base = background_colour * PIXEL_VALUE_OFFSET
		end

		if fg_solid then
			base = base + foreground_colour
		end

		local len = #text
		local line_offset = y * width + x - 1

		-- Go through the string, writing the new pixels
		for i = ( -x > 0 and -x or 0 ) + 1, ( len < ( width - x ) and len or width - x ) do
			local ch = byte[ sub( text, i, i ) ]

			-- A -1 for i is included in line_offset
			local index = line_offset + i

			local pixel = base
			local underneath = self[ index ]

			if not bg_solid then
				if background_colour == TRANSPARENT_BACKGROUND then
					-- Transparent to background
					local bg_under = underneath / PIXEL_VALUE_OFFSET
					pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
				else
					-- Transparent to foreground
					local fg_under = underneath % PIXEL_VALUE_OFFSET
					pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
				end
			end

			if not fg_solid then
				if foreground_colour == TRANSPARENT_BACKGROUND then
					-- Transparent to background
					local bg_under = underneath / PIXEL_VALUE_OFFSET
					pixel = pixel + bg_under - bg_under % 1
				else
					-- Transparent to foreground
					local fg_under = underneath % PIXEL_VALUE_OFFSET
					pixel = pixel + fg_under - fg_under % 1
				end
			end

			if ch == TRANSPARENT_CHARACTER then
				self[ index ] = pixel + underneath % 1
			else
				self[ index ] = pixel + ch
			end
		end
	end

	return self
end

--- Draw a solid colour rectangle border using two points.
-- @variant Draw-time transparency.
-- @param start_x			The x coordinate to start drawing at, 0-based
-- @param start_y			The y coordinate to start drawing at, 0-based
-- @param end_x				The x coordinate to end drawing at, 0-based
-- @param end_y				The y coordinate to end drawing at, 0-based
-- @param border_width		(Optional) The width of the border (larger values extending to the centre), defaults to 1
-- @param background_colour	(Optional) The background colour to fill the rectangle border with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the rectangle border with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the rectangle border with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_rectangle_from_points_v_dtt( start_x, start_y, end_x, end_y, border_width,
                                                     background_colour, foreground_colour, character )
	border_width = border_width or 1

	-- Starting values must be smaller than ending ones
	-- @todo Add debugging symbols
	if start_x > end_x then
		end_x, start_x = start_x, end_x
	end

	local w = self.width
	local almost_h = self.height - 1

	-- Avoid function calls (max()) where possible
	start_x = start_x < 0 and 0 or start_x
	end_x   = end_x > w - 1 and w - 1 or end_x

	if end_x < start_x then
		start_x, end_x = end_x, start_x
	end

	start_y = start_y < 0 and 0 or start_y
	end_y   = end_y > almost_h and almost_h or end_y

	if end_y < start_y then
		start_y, end_y = end_y, start_y
	end

	if start_y > end_y then
		end_y, start_y = start_y, end_y
	end

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local new_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		new_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		new_pixel = new_pixel + foreground_colour
	end

	if ch_solid then
		new_pixel = new_pixel + char_code
	end

	-- We limit the columns to avoid setting corners twice
	-- (see below (for/for/else)).
	-- We choose columns rather than lines, because lines
	-- can be more easily optimised (line_offset* can cache
	-- more information than column_offset*).
	local almost_start_y, almost_end_y = start_y + 1, end_y - 1

	for i = 0, border_width - 1 do
		local line_offset1 = ( start_y + i ) * w
		local line_offset2 = (   end_y - i ) * w

		local column_offset1 = start_x + i
		local column_offset2 =   end_x - i

		-- To avoid lots of duplicate code, we use a static
		-- for loop. For iterations 1 and 2, horizontal lines
		-- are being rendered (both above and below the
		-- rectangle's centre). For ii == 3 or ii == 4, columns
		-- are being drawn.
		for ii = 1, 4 do
			local _start, _end

			if ii == 1 or ii == 2 then
				_start, _end = start_x + i, end_x - i
			else
				-- Columns don't redraw corners
				_start, _end = almost_start_y + i, almost_end_y - i
			end

			for pos = _start, _end do
				local index

				if     ii == 1 then
					index = line_offset1 + pos
				elseif ii == 2 then
					index = line_offset2 + pos
				elseif ii == 3 then
					index = pos * w + column_offset1
				else
					index = pos * w + column_offset2
				end

				local pixel = new_pixel
				local underneath = self[ index ]

				if not bg_solid then
					if background_colour == TRANSPARENT_BACKGROUND then
						-- Transparent to background
						local bg_under = underneath / PIXEL_VALUE_OFFSET
						pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
					else
						-- Transparent to foreground
						local fg_under = underneath % PIXEL_VALUE_OFFSET
						pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
					end
				end

				if not fg_solid then
					if foreground_colour == TRANSPARENT_BACKGROUND then
						-- Transparent to background
						local bg_under = underneath / PIXEL_VALUE_OFFSET
						pixel = pixel + bg_under - bg_under % 1
					else
						-- Transparent to foreground
						local fg_under = underneath % PIXEL_VALUE_OFFSET
						pixel = pixel + fg_under - fg_under % 1
					end
				end

				if not ch_solid then
					pixel = pixel + underneath % 1
				end

				self[ index ] = pixel
			end
		end
	end

	return self
end

--- Draw a solid colour rectangle border using one point, width and height.
-- @variant Draw-time transparency.
-- @param x					The x coordinate to start drawing at, 0-based
-- @param y					The y coordinate to start drawing at, 0-based
-- @param width				The width of the rectangle
-- @param height			The height of the rectangle
-- @param ...				Any other arguments passed to :draw_rectangle_from_points_v_dtt()
-- @return Tail call of self:draw_rectangle_from_points_v_rtt()
-- @see buffer_methods:draw_rectangle_from_points_v_dtt
function buffer_methods:draw_rectangle_v_dtt( x, y, width, height, ... )
	-- @todo Localise :draw_rectangle_from_points
	return self:draw_rectangle_from_points_v_dtt( x, y, x + width - 1, y + height - 1, ... )
end

--- Draw a circle outline using the centre point and radius, without corrections for CC rectangular pixels.
-- @variant Draw-time transparency.
-- @param centre_x			The x coordinate of the circle's centre, 0-based
-- @param centre_y			The y coordinate of the circle's centre, 0-based
-- @param radius			The radius of the circle
-- @param background_colour	(Optional) The background colour to fill the circle outline with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the circle outline with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the circle outline with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_circle_raw_v_dtt( centre_x, centre_y, radius,
                                               background_colour, foreground_colour, character )
	local len = self.length
	local w = self.width

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local new_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		new_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		new_pixel = new_pixel + foreground_colour
	end

	if ch_solid then
		new_pixel = new_pixel + char_code
	end

	local x = radius
	local y = 0
	local err = 0

	local pixels = {}

	-- Generate points for the first octant, going counterclockwise
	-- and mirroring them to other octants
	while x >= y do
		local cypy = ( centre_y + y ) * w
		local cypy_in = cypy >= 0 and cypy <= len

		local cypx = ( centre_y + x ) * w
		local cypx_in = cypx >= 0 and cypx <= len

		local cymx = ( centre_y - x ) * w
		local cymx_in = cymx >= 0 and cymx <= len

		local cymy = ( centre_y - y ) * w
		local cymy_in = cymy >= 0 and cymy <= len


		local cxpx = centre_x + x
		local cxpx_in = cxpx >= 0 and cxpx <= w

		local cxpy = centre_x + y
		local cxpy_in = cxpy >= 0 and cxpy <= w

		local cxmy = centre_x - y
		local cxmy_in = cxmy >= 0 and cxmy <= w

		local cxmx = centre_x - x
		local cxmx_in = cxmx >= 0 and cxmx <= w


		pixels[ cypx + cxpy ] = cypx_in and cxpy_in or nil
		pixels[ cypx + cxmy ] = cypx_in and cxmy_in or nil

		pixels[ cypy + cxpx ] = cypy_in and cxpx_in or nil
		pixels[ cypy + cxmx ] = cypy_in and cxmx_in or nil

		pixels[ cymy + cxpx ] = cymy_in and cxpx_in or nil
		pixels[ cymx + cxpy ] = cymx_in and cxpy_in or nil

		pixels[ cymy + cxmx ] = cymy_in and cxmx_in or nil
		pixels[ cymx + cxmy ] = cymx_in and cxmy_in or nil

		y = y + 1
		err = err + 1 + 2 * y

		if 2 * ( err - x ) + 1 > 0 then
			x = x - 1
			err = err + 1 - 2 * x
		end
	end

	for index in next, pixels do
		local pixel = new_pixel
		local underneath = self[ index ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ index ] = pixel
	end

	return self
end

--- Draw a circle outline using the centre point and radius, with corrections for CC rectangular pixels.
-- @variant Draw-time transparency.
-- @param centre_x			The x coordinate of the circle's centre, 0-based
-- @param centre_y			The y coordinate of the circle's centre, 0-based
-- @param radius			The radius of the circle
-- @param background_colour	(Optional) The background colour to fill the circle outline with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the circle outline with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the circle outline with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_circle_v_dtt( centre_x, centre_y, radius, background_colour, foreground_colour, character )
	local len = self.length
	local w = self.width

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local new_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		new_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		new_pixel = new_pixel + foreground_colour
	end

	if ch_solid then
		new_pixel = new_pixel + char_code
	end

	local x = radius
	local y = 0
	local err = 0

	local pixels = {}

	-- Generate points for the first octant, going counterclockwise
	-- and mirroring them to other octants
	while x >= y do
		local cypy = ( centre_y + round( ( 2 / 3 ) * y ) ) * w
		local cypy_in = cypy >= 0 and cypy <= len

		local cypx = ( centre_y + round( ( 2 / 3 ) * x ) ) * w
		local cypx_in = cypx >= 0 and cypx <= len

		local cymx = ( centre_y - round( ( 2 / 3 ) * x ) ) * w
		local cymx_in = cymx >= 0 and cymx <= len

		local cymy = ( centre_y - round( ( 2 / 3 ) * y ) ) * w
		local cymy_in = cymy >= 0 and cymy <= len


		local cxpx = centre_x + x
		local cxpx_in = cxpx >= 0 and cxpx <= w

		local cxpy = centre_x + y
		local cxpy_in = cxpy >= 0 and cxpy <= w

		local cxmy = centre_x - y
		local cxmy_in = cxmy >= 0 and cxmy <= w

		local cxmx = centre_x - x
		local cxmx_in = cxmx >= 0 and cxmx <= w


		pixels[ cypx + cxpy ] = cypx_in and cxpy_in or nil
		pixels[ cypx + cxmy ] = cypx_in and cxmy_in or nil

		pixels[ cypy + cxpx ] = cypy_in and cxpx_in or nil
		pixels[ cypy + cxmx ] = cypy_in and cxmx_in or nil

		pixels[ cymy + cxpx ] = cymy_in and cxpx_in or nil
		pixels[ cymx + cxpy ] = cymx_in and cxpy_in or nil

		pixels[ cymy + cxmx ] = cymy_in and cxmx_in or nil
		pixels[ cymx + cxmy ] = cymx_in and cxmy_in or nil

		y = y + 1
		err = err + 1 + 2 * y

		if 2 * ( err - x ) + 1 > 0 then
			x = x - 1
			err = err + 1 - 2 * x
		end
	end

	for index in next, pixels do
		local pixel = new_pixel
		local underneath = self[ index ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ index ] = pixel
	end

	return self
end

--- Draw a filled ellipse.
--	Sorry for the inconsistency here, this code has been borrowed
--	from Xenthera's graphics library
-- @param x					The x coordinate of the ellipse's centre
-- @param y					The y coordinate of the ellipse's centre
-- @param width				The width  of the ellipse
-- @param height			The height of the ellipse
-- @param background_colour	(Optional) The background colour to fill the ellipse with, defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the ellipse with, defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the ellipse with, defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_filled_ellipse_v_dtt( x, y, width, height,
                                                   background_colour, foreground_colour, character )
	local self_width  = self.width
	local self_height = self.height
	local almost_w = self_width - 1

	local hh = height * height
	local ww = width * width
	local hhww = hh * ww
	local x0 = width - 1
	local delta_x = 0

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local new_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		new_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		new_pixel = new_pixel + foreground_colour
	end

	if ch_solid then
		new_pixel = new_pixel + char_code
	end

	local pixels = {}

	if y < self_height and y >= 0 then
		local offset = y * self_width

		local x_p_w = x + width
		local x_m_w = x - width

		for _x = x_m_w > 0 and x_m_w or 0, x_p_w < almost_w and x_p_w or almost_w do
			pixels[ offset + _x ] = true
		end
	end

	for _y = 0, height - 1 do
		local x1 = x0 - ( delta_x - 1 )
		local _y_yww = _y * _y * ww

		for _ = x1, 0, -1 do
			if x1 * x1 * hh + _y_yww <= hhww then
				break
			end

			x1 = x1 - 1
		end

		delta_x = x0 - x1
		x0 = x1

		local y_pos1 = y - _y
		local y_pos2 = y + _y

		if y_pos1 >= 0 and y_pos1 < self_height then
			y_pos1 = y_pos1 * self_width

			for _x = ( x - x0 ) > 0 and ( x - x0 ) or 0, ( ( x + x0 ) < ( self_width - 1 ) and ( x + x0 ) or self_width - 1 ) do
				pixels[ y_pos1 + _x ] = true
			end
		end

		if y_pos2 >= 0 and y_pos2 < self_height then
			y_pos2 = y_pos2 * self_width

			for _x = ( x - x0 ) > 0 and ( x - x0 ) or 0, ( ( x + x0 ) < ( self_width - 1 ) and ( x + x0 ) or self_width - 1 ) do
				pixels[ y_pos2 + _x ] = true
			end
		end
	end

	for index in next, pixels, nil do
		local pixel = new_pixel
		local underneath = self[ index ]

		if not bg_solid then
			if background_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
			end
		end

		if not fg_solid then
			if foreground_colour == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				pixel = pixel + bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				pixel = pixel + fg_under - fg_under % 1
			end
		end

		if not ch_solid then
			pixel = pixel + underneath % 1
		end

		self[ index ] = pixel
	end

	return self
end

--- Convert buffer data into a format useful for blitting to ComputerCraft terminal objects.
-- @param start_x	(Optional) The x coordinate to start reading data at, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate to start reading data at, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate to end reading data at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate to end reading data at, 0-based, defaults to self.height - 1
-- @return	An array of strings, where for i = 1, buf.height, 3 each i-th string contains
--			background colour data, each (i+1)-th string contains foreground colour data
--			and each (i+2)-th string represents the characters
function buffer_methods:cook_lines_v_dtt( start_x, start_y, end_x, end_y )
	local line = 1
	local lines = {}

	local w = self.width
	local h = self.height

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or w - 1
	end_y = end_y or h - 1

	-- The last pixel read, its colours and character
	local  last_pixel, bg, fg, ch

	for y = start_y, end_y do
		local line_offset = y * w

		local index = 1

		local background = {}
		local foreground = {}
		local characters = {}

		for x = start_x, end_x do
			local pixel = self[ line_offset + x ]

			if pixel ~= last_pixel then
				bg = pixel /     PIXEL_VALUE_OFFSET
				fg = pixel %     PIXEL_VALUE_OFFSET
				ch = pixel % 1 * PIXEL_VALUE_OFFSET

				last_pixel = pixel
			end

			background[ index ] = bg
			foreground[ index ] = fg
			characters[ index ] = ch

			index = index + 1
		end

		lines[ line     ] = string_char( unpack( background ) )
		lines[ line + 1 ] = string_char( unpack( foreground ) )
		lines[ line + 2 ] = string_char( unpack( characters ) )

		line = line + 3
	end

	return lines
end

--- Render the buffer to a CraftOS window object.
-- @param target	The window to render to
-- @param x			(Optional) The x coordinate in target to render self at, 1-based, defaults to self.x1 + 1
-- @param y			(Optional) The y coordinate in target to render self at, 1-based, defaults to self.y1 + 1
-- @param visible	(Optional) Whether the terminal object (if it's a window) should be visible after rendering.
--					If set, this option can significantly speed up the rendering process by avoiding window redraws.
-- @param start_x	(Optional) The x coordinate to start reading data (from self) at, 0-based, defaults to 0
-- @param start_y	(Optional) The y coordinate to start reading data (from self) at, 0-based, defaults to 0
-- @param end_x		(Optional) The x coordinate to end reading data (from self) at, 0-based, defaults to self.width - 1
-- @param end_y		(Optional) The y coordinate to end reading data (from self) at, 0-based, defaults to self.height - 1
-- @param palette	(Optional) A lookup table to use for converting colours,
--               	defaults to ComputerCraft's native colour mapping
-- @param dont_fix	(Optional) If true, the supplied palette won't be checked for correctness
--					It is recommended to use valid palettes (ones that contain entries for all known colours) along
--					with this flag, to ensure best performance
-- @return self
function buffer_methods:render_to_window_v_dtt( target, x, y, visible,
                                                start_x, start_y, end_x, end_y, palette, dont_fix )
	x = x or ( self.x1 and self.x1 + 1 or error( unable_to_set_optional_argument .. "'x': self.x1 is nil", 2 ) )
	y = y or ( self.y1 and self.y1 + 1 or error( unable_to_set_optional_argument .. "'y': self.y1 is nil", 2 ) )

	local setc, blit  = target.setCursorPos, target.blit
	local set_visible = target.setVisible

	if visible ~= nil and set_visible then
		set_visible( false )
	end

	-- Go through all lines of the buffer
	-- @todo Localise :cook_lines
	local lines = self:cook_lines_v_dtt( start_x, start_y, end_x, end_y, palette, dont_fix )

	local line = 0
	for i = 1, #lines, 3 do
		setc( x, y + line )
		blit( lines[ i + 2 ], lines[ i + 1 ], lines[ i ] )

		line = line + 1
	end

	if visible == true and set_visible then
		set_visible( true )
	end

	return self
end

--- Render the buffer to another buffer.
-- @param target				(Optional) The buffer to render to, defaults to self.parent
-- @param x						(Optional) The x coordinate in target to render self at, 0-based, defaults to self.x1
-- @param y						(Optional) The y coordinate in target to render self at, 0-based, defaults to self.y1
-- @param start_x				(Optional) The x coordinate in self to start rendering from, 0-based, defaults to 0
-- @param start_y				(Optional) The y coordinate in self to start rendering from, 0-based, defaults to 0
-- @param end_x					(Optional) The x coordinate in self to stop rendering at, 0-based, defaults to self.width - 1
-- @param end_y					(Optional) The y coordinate in self to stop rendering at, 0-based, defaults to self.height - 1
-- @return self
function buffer_methods:render_v_dtt( target, x, y, start_x, start_y, end_x, end_y )
	target = target or self.parent or error( unable_to_set_optional_argument .. "'target': self.parent is nil", 2 )
	x      = x      or self.x1     or error( unable_to_set_optional_argument .. "'x': self.x1 is nil", 2 )
	y      = y      or self.y1     or error( unable_to_set_optional_argument .. "'y': self.y1 is nil", 2 )

	local self_width = self.width
	local target_width = target.width

	start_x = start_x or 0
	start_y = start_y or 0
	end_x = end_x or self_width  - 1
	end_y = end_y or self.height - 1

	-- Respect borders of the render target
	start_x = start_x > -x and start_x or -x
	start_y = start_y > -y and start_y or -y
	end_x   = ( end_x < ( target_width  - x - 1 ) and end_x or target_width  - x - 1 )
	end_y   = ( end_y < ( target.height - y - 1 ) and end_y or target.height - y - 1 )

	-- Loop through all coordinates
	for _y = start_y, end_y do
		-- Cache the line position for both the target buffer and us
		local target_offset = ( _y + y ) * target_width + x
		local local_offset  = _y * self_width

		for _x = start_x, end_x do
			target[ target_offset + _x ] = self[ local_offset + _x ]
		end
	end

	return self
end

--- Draw a filled, solid colour rectangle using one point, width and height.
-- @param start_x			The x coordinate to start drawing at, 0-based
-- @param start_y			The y coordinate to start drawing at, 0-based
-- @param end_x				The x coordinate to end drawing at, 0-based
-- @param end_y				The y coordinate to end drawing at, 0-based
-- @param background_colour	(Optional) The background colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_BACKGROUND
-- @param foreground_colour	(Optional) The foreground colour to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_FOREGROUND
-- @param character			(Optional) The character to fill the rectangle (including borders) with,
--                         	defaults to DEFAULT_CHARACTER
-- @return self
function buffer_methods:draw_filled_rectangle_from_points_v_dtt( start_x, start_y, end_x, end_y,
                                                                 background_colour, foreground_colour, character )
	-- Starting values must be smaller than ending ones
	-- @todo Add debugging symbols
	if start_x > end_x then
		end_x, start_x = start_x, end_x
	end

	local w = self.width
	local almost_h = self.height - 1

	-- Avoid function calls (max()) where possible
	start_x = start_x < 0 and 0 or start_x
	end_x   = end_x > w - 1 and w - 1 or end_x

	if end_x < start_x then
		start_x, end_x = end_x, start_x
	end

	start_y = start_y < 0 and 0 or start_y
	end_y   = end_y > almost_h and almost_h or end_y

	if end_y < start_y then
		start_y, end_y = end_y, start_y
	end

	if start_y > end_y then
		end_y, start_y = start_y, end_y
	end

	background_colour = tonumber( background_colour ) or DEFAULT_BACKGROUND
	foreground_colour = tonumber( foreground_colour ) or DEFAULT_FOREGROUND

	local char_code = byte[ character or DEFAULT_CHARACTER ]

	local bg_solid = background_colour < TRANSPARENT_VALUES
	local fg_solid = foreground_colour < TRANSPARENT_VALUES

	-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
	local ch_solid = char_code ~= TRANSPARENT_CHARACTER

	local new_pixel = 0

	if  character         == TRANSPARENT_CHARACTER
	and foreground_colour == TRANSPARENT_FOREGROUND
	and background_colour == TRANSPARENT_BACKGROUND then
		return self
	end

	if bg_solid then
		new_pixel = background_colour * PIXEL_VALUE_OFFSET
	end

	if fg_solid then
		new_pixel = new_pixel + foreground_colour
	end

	if ch_solid then
		new_pixel = new_pixel + char_code
	end

	-- Go through all pixels of this rectangle and set them to new_pixel
	for _y = start_y, end_y do
		local line_offset = _y * w

		for _x = start_x, end_x do
			local index = line_offset + _x

			local pixel = new_pixel
			local underneath = self[ index ]

			if not bg_solid then
				if background_colour == TRANSPARENT_BACKGROUND then
					-- Transparent to background
					local bg_under = underneath / PIXEL_VALUE_OFFSET
					pixel = pixel + ( bg_under - bg_under % 1 ) * PIXEL_VALUE_OFFSET
				else
					-- Transparent to foreground
					local fg_under = underneath % PIXEL_VALUE_OFFSET
					pixel = pixel + ( fg_under - fg_under % 1 ) * PIXEL_VALUE_OFFSET
				end
			end

			if not fg_solid then
				if foreground_colour == TRANSPARENT_BACKGROUND then
					-- Transparent to background
					local bg_under = underneath / PIXEL_VALUE_OFFSET
					pixel = pixel + bg_under - bg_under % 1
				else
					-- Transparent to foreground
					local fg_under = underneath % PIXEL_VALUE_OFFSET
					pixel = pixel + fg_under - fg_under % 1
				end
			end

			if not ch_solid then
				pixel = pixel + underneath % 1
			end

			self[ index ] = pixel
		end
	end

	return self
end

--- Draw a filled, solid colour rectangle using one point, width and height.
-- @param x					(Optional) The x coordinate to start drawing at, 0-based, defaults to 0
-- @param y					(Optional) The y coordinate to start drawing at, 0-based, defaults to 0
-- @param width				(Optional) The width  of the rectangle, defaults to 0
-- @param height			(Optional) The height of the rectangle, defaults to 0
-- @param ...				Any other parameters passed to self:draw_filled_rectangle_from_points_v_dtt()
-- @see buffer_methods:draw_filled_rectangle_from_points_v_dtt
-- @return self
function buffer_methods:draw_filled_rectangle_v_dtt( x, y, width, height, ... )
	x = x or 0
	y = y or 0
	width  = width  or 0
	height = height or 0

	return self:draw_filled_rectangle_from_points_v_dtt( x, y, x + width - 1, y + height - 1, ... )
end

--- Write text to the buffer using the specified colours.
--	text, background_colours and foreground_colours have to
--	be of the same length
-- @param x						The x coordinate to write at, 0-based
-- @param y						The y coordinate to write at, 0-based
-- @param text					The text to write
-- @param background_colours	The background colours to use, in paint format (characters)
-- @param foreground_colours	The foreground colours to use, in paint format (characters)
-- @return self
function buffer_methods:blit_v_dtt( x, y, text, background_colours, foreground_colours )
	x = tonumber( x ) or error( expected_number_for .. "'x'", 2 )
	y = tonumber( y ) or error( expected_number_for .. "'y'", 2 )

	local w = self.width

	local text_length = #text

	if text_length ~= #background_colours or text_length ~= #foreground_colours then
		error( "'text', 'background_colours', and 'foreground_colours' all have to be of the same length", 2 )
	end

	background_colours = gsub( background_colours, "%s", "f" )
	foreground_colours = gsub( foreground_colours, "%s", "0" )

	local offset = y * w + x - 1
	text_length  = ( text_length < ( w - x ) and text_length or w - x )

	for i = ( -x > 0 and -x or 0 ) + 1, text_length do
		-- A -1 for i is included in offset
		local index = offset + i

		local bg = colour_lookup[ sub( background_colours, i, i ) ]
		local fg = colour_lookup[ sub( foreground_colours, i, i ) ]
		local ch = byte[  sub( text, i, i ) or DEFAULT_CHARACTER  ]

		local underneath = self[ index ]

		if bg >= TRANSPARENT_VALUES then
			print( "bg transparent" )
			if bg == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				bg = bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				bg = fg_under - fg_under % 1
			end
		end

		if fg >= TRANSPARENT_VALUES then
			if fg == TRANSPARENT_BACKGROUND then
				-- Transparent to background
				local bg_under = underneath / PIXEL_VALUE_OFFSET
				fg = bg_under - bg_under % 1
			else
				-- Transparent to foreground
				local fg_under = underneath % PIXEL_VALUE_OFFSET
				fg = fg_under - fg_under % 1
			end
		end

		-- @warn Works without dividing TRANSPARENT_CHARACTER as long as it's 0
		if ch == TRANSPARENT_CHARACTER then
			ch = underneath % 1
		end

		self[ index ] = bg * PIXEL_VALUE_OFFSET + fg + ch
	end

	return self
end


return buffer_methods
