
local surface = dofile "battleground/Surface2/target/surface"

local n = surface.create

local clock = os.clock
local limit = 10000

local start = clock()
for _ = 1, limit do
	local _ = n( 51, 19 )
end

local total = clock() - start

print( "case", '"realistic"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"Constructor calls per second for a 51x19 surface."' )

limit = 1000
start = clock()

for _ = 1, limit do
	local _ = n( 100, 100 )
end

total = clock() - start

print( "case", '"scale test"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"Constructor calls per second for a 100x100 surface."' )

