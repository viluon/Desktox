
local surface = dofile "battleground/Surface2/target/surface"

local surf = surface.create( 51, 19 )
local clear = surf.clear

local clock = os.clock
local limit = 10000

local start = clock()
for _ = 1, limit do
	clear( surf )
end

local total = clock() - start

print( "case", '"realistic"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"`surf:clear()` calls per second for a 51x19 surface."' )

surf = surface.create( 100, 100 )

limit = 1000
start = clock()

for _ = 1, limit do
	clear( surf )
end

total = clock() - start

print( "case", '"scale test"' )
print( "time", total )
print( "runs", limit )
print( "desc", '"`surf:clear()` calls per second for a 100x100 surface."' )
