
# Desktox
Desktox is a set of pure Lua graphics libraries for [ComputerCraft](http://computercraft.info). It aims to be fast, hackable, and easy to use.

## Libraries
Desktox employs a modular design.

### `buffer`
The cornerstone of Desktox. This swift framebuffer library offers a multitude of basic drawing and rendering functions as well as a CraftOS `window`-like interface.

### `handler`
A tail-recursive event handling library (allows for arbitrarily deep UI hierarchy trees).

### `gui`
A GUI library which depends on `buffer` and `handler`, contains a set of graphical UI elements.

### `utils`
Various utility functions shared among the other modules.

## Documentation
The source code is documented in an [LDoc](https://github.com/stevedonovan/LDoc/) style. In the future, the documentation will be available online.

## License
Desktox is a work of Andrew Kvapil, also known as viluon, copyleft 2016, all rights reversed. All packages of Desktox are available under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/) unless stated otherwise.
There is one notable exception: the `require.lua` file in the project's root is copyright (c) 2014 Odd Straaboe (oddstr13@openshell.no), licensed under [the MIT License](http://opensource.org/licenses/MIT).
