
local hererocks = "python hr.py "
local rocks = { "luafilesystem" }

--- Fix the invalid identifier in $env/bin/activate.
-- @param path	The path to the activation script
-- @return nil
local function fix_identifier( path )
	local f = io.open( path, "r" )
	local contents = f:read( "*a" )
	f:close()

	f = io.open( path, "w" )
	f:write( ( contents:gsub( "deactivate%-lua", "deactivate_lua" ) ) )
	f:close()
end

--- Run a benchmarking script and parse the benchmark output.
-- @param path	The command to actually run (e.g. `lua foo.lua`)
-- @return The status code, followed by the test data if zero
local function run_and_parse( path )
	local process = io.popen( path, "r" )
	local output = process:read( "*a" )
	process:close()

	print( output )

	local results = {}
	local n_results = 0

	for case, pos in output:gmatch( "case%s+\"([^\"]*)\"%s+()" ) do
		local time, runs, desc = output
			:match( "time%s+([%d%.]+)%s+runs%s+(%d+)%s+desc%s+\"([^\"]*)\"", pos )

		time, runs = tonumber( time ), tonumber( runs )

		if not time or not runs then
			return -1
		end

		-- We'll be converting to JSON, Lua indices start from 1
		results[ n_results ] = { scenario = case; time = time; runs = runs; desc = desc }
		n_results = n_results + 1
	end

	results.n_scenarios = n_results

	return 0, results
end

--- Initialise a Lua installation, using hererocks.
-- @param env	The path to the local environment
-- @param version	The version of Lua to install
-- @return nil
local function init_Lua( env, version )
	os.execute( hererocks .. env .. " -" .. version .. " -rlatest" )

	local activate = env .. "/bin/activate"
	fix_identifier( activate )
	os.execute( "chmod +x " .. activate )
end

--- Install required LuaRocks.
-- @param env	The path to the local environment
-- @param version	The version of LuaRocks to use
-- @return nil
local function petrify( env, version )
	local exe = env .. "/bin/luarocks-" .. version .. " "

	local p = io.popen( exe .. "list --local --tree=" .. env, "r" )
	local output = p:read( "*a" )
	p:close()

	local installed = {}
	output:gsub( "\n([%w_]+)\n%s*([%w%.%-_]+)%s+%(installed%)%s+%-%s+", function( rock, v )
		installed[ rock ] = v
	end )

	-- Go through the list of required rocks, installing them along the way
	for i = 1, #rocks do
		local rock = rocks[ i ]

		if not installed[ rock ] then
			os.execute( exe .. "install --local " .. rock .. " --tree=" .. env )
		else
			print( "petrify: " .. rock .. " (" .. installed[ rock ] .. ") is already installed." )
		end
	end
end

local platforms = {
	{
		name = "lua5.1";
		setup = function( env )
			init_Lua( env, "l5.1" )
			petrify ( env,  "5.1" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
		end;
		run = function( path, env )
			os.execute( "source "   .. env .. "/bin/activate" )
			return run_and_parse( env .. "/bin/lua -lluarocks.loader -llfs " .. path )
		end;
	};
	{
		name = "lua5.2";
		setup = function( env )
			init_Lua( env, "l5.2" )
			petrify ( env,  "5.2" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
		end;
		run = function( path, env )
			os.execute( "source "   .. env .. "/bin/activate" )
			return run_and_parse( env .. "/bin/lua -lluarocks.loader -llfs " .. path )
		end;
	};
	{
		name = "lua5.3";
		setup = function( env )
			init_Lua( env, "l5.3" )
			petrify ( env,  "5.3" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
		end;
		run = function( path, env )
			os.execute( "source "   .. env .. "/bin/activate" )
			return run_and_parse( env .. "/bin/lua -lluarocks.loader -llfs " .. path )
		end;
	};
	{
		name = "luajit2.0";
		setup = function( env )
			init_Lua( env, "j2.0" )
			petrify ( env,  "5.1" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
		end;
		run = function( path, env )
			os.execute( "source "   .. env .. "/bin/activate" )
			return run_and_parse( env .. "/bin/lua -lluarocks.loader -llfs " .. path )
		end;
	};
	{
		name = "luajit2.1";
		setup = function( env )
			init_Lua( env, "j2.1" )
			petrify ( env,  "5.1" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
		end;
		run = function( path, env )
			os.execute( "source "   .. env .. "/bin/activate" )
			return run_and_parse( env .. "/bin/lua -lluarocks.loader -llfs " .. path )
		end;
	};
	{
		name = "terra";
		optional = function()
			return os.execute( "terra - < /dev/null" ) == 0
		end;
		setup = function( env )
			init_Lua( env, "j2.0" )
			petrify ( env,  "5.1" )
		end;
		info = function( env )
			os.execute( "pwd" )
			os.execute( env .. "/bin/lua -v" )
			os.execute( "terra --version" )
		end;
		run = function( path, env )
			local tmp = env .. "/temp.lua"
			os.execute( [[echo '
_ENV = nil
utf8 = nil
bit32 = nil
luarocks = nil
require "luarocks.loader"
require "lfs"
' > ]] .. tmp )
			os.execute( "cat " .. path .. " >> " .. tmp )
			os.execute( env .. "/bin/luarocks-5.1 path > " .. env .. "/path_setup.tmp" )
			return run_and_parse( "source " .. env .. "/path_setup.tmp && terra " .. tmp )
		end;
	};
}

-- Fetch hardware information
local hwinfo_sources = {
	"cat /proc/version";
	"grep -i 'model name' /proc/cpuinfo";
	"grep -i 'memtotal'   /proc/meminfo";
}

local hwinfo = ""
for _, source in ipairs( hwinfo_sources ) do
	local p = io.popen( source, "r" )
	hwinfo = hwinfo .. "\n`" .. source .. "`\n" .. p:read( "*a" )
	p:close()
end

local results = {}
for _, command in pairs {
	"uname -n",							-- Machine name
	"whoami",							-- User name
	"pwd",								-- Current directory
	"date -u +%s",						-- Seconds since epoch
	"git log --format=%H -1",			-- Latest commit (full SHA)
	"git rev-parse --abbrev-ref HEAD"	-- Current branch
} do
	local p = io.popen( command, "r" )
	local out = p:read( "*a" )
	-- Let's close the handle before any errors can
	-- happen, just in case.
	p:close()

	results[ #results + 1 ] = out:sub( 1, -2 )
end

local machine_name, user_name, path, time, commit, branch = unpack( results )

return {
	platforms = platforms;
	n_platforms = #platforms;

	meta = {
		path = path:gsub( "/home/" .. user_name, "~" );
		time = tonumber( time ) or error( "Cannot parse `date` output" );
		user = user_name;
		commit = commit;
		branch = branch;
		hwinfo = hwinfo;
		machine = machine_name;
	};
}
